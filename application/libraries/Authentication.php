<?php
class Authentication {
	function Authentication() {
		$CI =& get_instance();
		$CI->load->library('session');
//		require_once "application/vo/Teacher.php";
//		require_once "application/vo/Admin.php";
//		require_once "application/vo/Student.php";
	}
	function getUser() {
	
	/*	$CI =& get_instance();
		$CI->load->helper('cookie');
		$CI->load->model('Usermodel');
		$user = false;
		$id = $CI->session->userdata('userId');

		if (isset($id)) {
			if (!$id) {
				return $user;
			}
			$user = $CI->Usermodel->getUserById($id);
		}
		return $user;*/
	}
	
	function getUserAndPassword() {
	
	/*	$CI =& get_instance();
		$CI->load->helper('cookie');
		$CI->load->model('Usermodel');
		$user = false;
		$id = $CI->session->userdata('userId');
		$password = $CI->session->userdata('userPassword');

		if (isset($id) && isset($password)) {
			if (!$id || !$password) {
				return $user;
			}
			$user = $CI->Usermodel->getUserById($id);
		}
		return $user;*/
		
	}
	
	function isTeacher() {
		$user = $this->getUser();
		return ($user && $user->userTypeId == 1);
	}
	function isStudent() {
		$user = $this->getUserAndPassword();
		return ($user && $user->userTypeId == 2);
	}
	function isAdmin() {
		$user = $this->getUser();
		return ($user && $user->userTypeId == 3);
	}
	
	function loginTeacher($teacher) {	
		$CI = & get_instance();
		$CI->load->helper('cookie');
		$CI->load->model('Teachermodel');
		
		$loginTeacher =	$teacher;
		$loginTeacher->userName = $loginTeacher->scootleId;
		$loginTeacher->firstName = 'teacherFirstName';
		$loginTeacher->surName = 'teacherSurName';
		$loginTeacher->displayInformation = false;
		
		$teacher = $CI->Teachermodel->getTeacherFromCredentials($teacher);
	
		if (!$teacher)
		{
			$teacher = $CI->Teachermodel->insertTeacher($loginTeacher);
		}
		$CI->session->set_userdata('userId', $teacher->id);	
		return $teacher;
	}
	function loginAdmin($admin) {
		$CI = & get_instance();
		$CI->load->helper('cookie');
		$CI->load->model('Adminmodel');

				
		$admin = $CI->Adminmodel->getAdminFromCredentials($admin);
		if (!$admin)
			return false;
		$CI->session->set_userdata('userId', $admin->id);	
		return $admin;
	}
	function loginStudent($student) {
		$CI = & get_instance();
		$CI->load->helper('cookie');
		$CI->load->model('Studentmodel');
		
		$result = 0;
		
		if (!$student->password)
			return false;
		
		$student = $CI->Studentmodel->getStudentFromCredentials($student);
		if (!$student)
			return false;
		
		$CI->session->set_userdata('userId', $student->id);	
		$CI->session->set_userdata('userPassword', $student->password);	
		
		
		return $student;
	}
	
	
	function logout(){	
		$CI = & get_instance();
		$CI->session->unset_userdata('userId');
		$CI->session->unset_userdata('userPassword');
	}
}
?>