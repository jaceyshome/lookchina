<?php
class ResourceModel extends CI_Model {
  function ResourceModel(){
		parent::__construct();
		$this->load->database();
		$this->load->library('TimeUtil');

		require_once "application/vo/Resource.php";
		require_once "application/vo/Dynasty.php";
		require_once "application/vo/MapConfig.php";
		require_once "application/vo/Category.php";
		require_once "application/vo/ResourceType.php";
		require_once "application/vo/Version.php";
		require_once "application/vo/Options.php";
  }

	function getLastestVersion(){
		$sql = "SELECT * FROM version ORDER BY name desc LIMIT 1";

		$query = $this->db->query($sql);

		$versions = array();
		foreach($query->result() as $row) {
			$version = Version::fromRow($row);
			array_push($versions, $version);
		}
		return $versions[0];

	}

	function listResources(){
		$sql = "	SELECT * FROM resource
						WHERE dynastyId IS NOT NULL
						ORDER BY dynastyId,
										 resourceCategoryId,
										 resourceTypeId";

		$query = $this->db->query($sql);

		$resources = array();
		foreach($query->result() as $row) {
			$resource = Resource::fromRow($row);
			array_push($resources, $resource);
		}
		return $resources;
	}


	function listDynasties(){

		$dynasties = $this->listDynastyTable();

		return $dynasties;
	}

	function listDynastyTable(){

		$sql = "SELECT * FROM dynasty ORDER BY id";

		$query = $this->db->query($sql);

		$dynasties = array();
		foreach($query->result() as $row) {
			$dynasty = Dynasty::fromRow($row);

			if($dynasty->enable == 1){
				$dynasty->categories = $this->listCategoryTableForDynasties($dynasty->id);
				$dynasty->mapConfigs = $this->listDynastyPeriodMapConfigs($dynasty->id);
			}

			array_push($dynasties, $dynasty);
		}

		return $dynasties;
	}

	function listResourceCategoryTable(){

		$sql = "SELECT * FROM category ORDER BY id";
		$query = $this->db->query($sql);
		$categories = array();

		foreach($query->result() as $row) {
			$category = Category::fromRow($row);
			array_push($categories, $category);
		}

		return $categories;

	}

	function listCategoryTableForDynasties($dynastyId){

		$sql = "SELECT * FROM category ORDER BY id";
		$query = $this->db->query($sql);
		$categories = array();

		foreach($query->result() as $row) {
			$category = Category::fromRow($row);

			$category->resources = $this->listDynastyResources($dynastyId, $category->id);

			array_push($categories, $category);
		}

		return $categories;

	}

	function listResourceTypeTable(){
		$sql = "SELECT * FROM resourcetype ORDER BY id";

		$query = $this->db->query($sql);
		$resourceTypes = array();

		foreach($query->result() as $row) {
			$resourceType = ResourceType::fromRow($row);
			array_push($resourceTypes, $resourceType);
		}

		return $resourceTypes;
	}


	function listDynastyResources($dynastyId, $categoryId){

		$sql = "SELECT
							r.id,
							r.name,
							r.description,
							r.introduction,
							r.editDateTime,
							r.resourceCategoryId,
							r.thumbUrl,
							r.dynastyId,
							r.x ,
							r.y,
							r.z,
							r.resourceTypeId,
							c.iconName
						FROM resource AS r
						LEFT JOIN category AS c ON resourceCategoryId = c.id
						WHERE dynastyId = '$dynastyId'
						AND resourceCategoryId = '$categoryId'
						ORDER BY dynastyId,resourceCategoryId,resourceTypeId";

		$query = $this->db->query($sql);

		$resources = array();
		foreach($query->result() as $row) {
			$resource = Resource::fromRow($row);
			array_push($resources, $resource);
		}

		return $resources;

	}

	function listDynastyPeriodMapConfigs($dynastyId){

		$sql = "SELECT
							*
						FROM mapconfig
						WHERE dynastyId = '$dynastyId'
						";

		$query = $this->db->query($sql);

		$mapConfigs = array();
		foreach($query->result() as $row) {
			$mapConfig = MapConfig::fromRow($row);
			array_push($mapConfigs, $mapConfig);
		}

		return $mapConfigs;
	}

	function loadDynastyIntroAudios($dynasty){
		if (isset($dynasty->introduction)){

			$introduction = $dynasty->introduction;
			$sentences = array();
			$sentences = preg_split('/\.|\?|!/',$introduction);

			var_dump($sentences);


			return true;

		}else{
			return false;
		}
	}

}
?>

