<?php
class latestversion extends MY_Controller {

	function latestversion()
	{
		parent::MY_Controller();
		require_once "application/vo/Version.php";
	}

	function index()
	{
		$this->_dispatch();
	}

	function _get()
	{
		$this->load->library('JSONOutput');
		$this->load->model('ResourceModel');

		/*$user = $this->authentication->getUser();
		if (!$user)	{
			return $this->returnError(401, 'unauthorised');
		}*/

		$version = $this->ResourceModel->getLastestVersion();
		$this->jsonoutput->output($version);
	}
}
?>