<?php
class adminLogin extends MY_Controller {

	function adminLogin()
	{
		parent::MY_Controller();
	}
	
	function index()
    {
       $this->_dispatch();
    }
	
	function _post()
	{
		$this->load->library('JSONOutput');
		$this->load->library('Authentication');
		$admin = $this->authentication->isAdmin();
		if ($admin)	{
			return $this->returnError(401, 'unauthorised', 'you are already logged in');
		}
		$_POST  = array();  
		if($_SERVER['REQUEST_METHOD'] == 'POST') {  
			$_POST = file_get_contents('php://input');
		}  
		$json = json_decode($_POST, true);
		
		//validate and save project
		require_once "application/libraries/Schema.class.php";
		require_once "application/libraries/SchemaValidator.class.php";
		$schema = (new Schema('application/libraries/jsonSchemas/adminLogin.json'));
		
		try{
			// validation
			$validator = (new SchemaValidator($schema, $json));
			if ($validator->valid()) {
				$admin = Admin::fromJSON($json);
				//var_dump($admin);
				//die();
				$admin = $this->authentication->loginAdmin($admin);
				if (!$admin) {
					return $this->returnError(401, 'Unauthorised', "Incorrect admin login"); 
				}
				$this->jsonoutput->output($admin);
			} else {
				$errors = $validator->getFailedRules();
				$message = $errors[0]['error']['message'];
				$input = $errors[0]['error']['input'];
				return $this->returnError(400, 'bad request',$message); 
			}
		}catch (Exception $e) {
			return $this->returnError(400, $e->getMessage());
		}
	}
}
?>