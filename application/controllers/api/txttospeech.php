<?php
class txtToSpeech extends MY_Controller {

	function txtToSpeech()
	{
		parent::MY_Controller();
		require_once "application/vo/IntroAudios.php";
	}

	function index()
	{
		$this->_dispatch();
	}

	function _post()
	{
		$this->load->library('JSONOutput');
		$this->load->library('Authentication');

		$_POST = array();
		if($_SERVER['REQUEST_METHOD'] == 'POST') {
			$_POST = file_get_contents('php://input');
		}

		$json = json_decode($_POST, true);

		$introAudios = IntroAudios::fromJSON($json);

		$this->jsonoutput->output($introAudios);
		return;

		foreach($introAudios as $introAudio){
			$querystring = http_build_query(array(
				//you can try other language codes here like en-english,hi-hindi,es-spanish etc
				"tl" => "en",
				"q" => $introAudio->text
			));

			$filename = $introAudio->audio;

			if ($soundfile = file_get_contents("http://translate.google.com/translate_tts?".$querystring))
			{
				file_put_contents($filename,$soundfile);
			}else{
				return $this->returnError(401, 'Unauthorised', "Failt to get intro audio file:".$filename.$introAudio->text);
			}

		}

		$this->jsonoutput->output($introAudios);

	}
}
?>