<?php
class resourcecategories extends MY_Controller {

	function resourcecategories()
	{
		parent::MY_Controller();
		require_once "application/vo/Category.php";
	}

	function index()
	{
		$this->_dispatch();
	}

	function _get()
	{
		$this->load->library('JSONOutput');
		$this->load->model('ResourceModel');
		$this->load->library('Authentication');

		/*$user = $this->authentication->getUser();
		if (!$user)	{
			return $this->returnError(401, 'unauthorised');
		}*/

		$resources = $this->ResourceModel->listResourceCategoryTable();
		$this->jsonoutput->output($resources);
	}
}
?>