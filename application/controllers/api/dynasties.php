<?php
class dynasties extends MY_Controller {

	function dynasties()
	{
		parent::MY_Controller();
		require_once "application/vo/Dynasty.php";
	}

	function index()
	{
		$this->_dispatch();
	}

	function _get()
	{
		$this->load->library('JSONOutput');
		$this->load->model('ResourceModel');
		$this->load->library('Authentication');

		/*$user = $this->authentication->getUser();
		if (!$user)	{
			return $this->returnError(401, 'unauthorised');
		}*/

		$dynasties = $this->ResourceModel->listDynasties();
		$this->jsonoutput->output($dynasties);
	}
}
?>