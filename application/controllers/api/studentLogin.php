<?php
class studentLogin extends MY_Controller {

	function studentLogin()
	{
		parent::MY_Controller();
	}
	
	function index()
    {
       $this->_dispatch();
    }
	
	function _post()
	{
		$this->load->library('JSONOutput');
		$this->load->library('Authentication');
		$this->load->model('Studentmodel');
		$this->load->model('Usermodel');

		
		$student = $this->authentication->isStudent();

		$_POST  = array();  
		if($_SERVER['REQUEST_METHOD'] == 'POST') {  
			$_POST = file_get_contents('php://input');
		}  
		$json = json_decode($_POST, true);
		
		//teacher launching challenges
		$isTeacher = $this->authentication->isTeacher();
		$isAdmin = $this->authentication->isAdmin();
		if ($isTeacher || $isAdmin) {
			$allowedUser = $this->authentication->getUser();
			return $this->jsonoutput->output($allowedUser);
		}
				
		//validate and save project
		$student = User::fromJSON($json);
		$student = $this->Studentmodel->getStudentFromCredentials($student);
		if (!$student) {
			return $this->returnError(401, 'Unauthorised', "Incorrect student login"); 
		}
		
		//(1) check student unique name
		
		$result = $this->authentication->loginStudent($student);
		$result = $this->Studentmodel->checkStudentUniqueFullName($student);
		
		if (!$result){
			//return $this->returnError(404, 'Conflict', "not unique first name or last name");
			$student->conflict = true;
			
		}
		/*else{
			//$result = $this->authentication->loginStudent($student);
			if (!$result) {
				return $this->returnError(401, 'Unauthorised', "Incorrect student login"); 
			}
		
			$this->jsonoutput->output($student);
		} 
*/
		
		$this->jsonoutput->output($student);
		
		
	}
}
?>