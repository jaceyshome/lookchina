<?php
class resourcetypes extends MY_Controller {

	function resourcetypes()
	{
		parent::MY_Controller();
		require_once "application/vo/ResourceType.php";
	}

	function index()
	{
		$this->_dispatch();
	}

	function _get()
	{
		$this->load->library('JSONOutput');
		$this->load->model('ResourceModel');
		$this->load->library('Authentication');

		/*$user = $this->authentication->getUser();
		if (!$user)	{
			return $this->returnError(401, 'unauthorised');
		}*/

		$resources = $this->ResourceModel->listResourceTypeTable();
		$this->jsonoutput->output($resources);
	}
}
?>