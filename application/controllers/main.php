<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	function index()
	{
		//load helpers
		$this->load->helper('url');
		parse_str($_SERVER['QUERY_STRING'], $parameters);
		if (isset($parameters['tmid']))$tmId = $parameters['tmid'];
		if (isset($parameters['footerid']))$footerId = $parameters['footerid'];
		if (isset($parameters['section']))$sectionName = $parameters['section'];

		$this->load->helper('form');
		
		$this->load->library('Authentication');
//		$this->load->model('Resourcemodel');
//		$user = $this->authentication->getUser();
//		$isStudent = $this->authentication->isStudent();
//		if ($isStudent) {
//			$user = false;
//		}
		
		$viewdata["user"] = false;

		//render
		$this->load->view('main', $viewdata);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */