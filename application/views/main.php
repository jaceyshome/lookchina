<!DOCTYPE HTML>
<html lang="en" id="ng-app" xml:lang="en" xmlns:ng="http://angularjs.org">
<head>
	<base href="<?php echo site_url();?>" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="fragment" content="!" />
	<title>Look China</title>
	<script type="text/javascript">var site_url = "<?php echo site_url();?>";</script>
	<script type="text/javascript">var base_url = "<?php echo base_url();?>";</script>
	<link  rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" type="text/css" charset="utf-8" />
	<!--<script src="//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
	<script type="text/javascript">
		WebFontConfig = {
			custom: { families: ['hooge0665'],
				urls: [ '<?php /*echo site_url('assets/css/font.css')*/?>' ] }
		};
	</script>-->
</head>

<body ng-controller="MainScreenCtrl" ng-clock>

	<div id="earthContainer" ></div>
	<canvas id="mainScreen" width="1024" height="680"></canvas>
	<canvas id="alphaCanvas" width="1024" height="680"></canvas>
	<canvas id="betaCanvas" width="1024" height="680"></canvas>
	<ul class="videoContainer" ng-cloak class="ng-cloak">
		<li ng-show="false" ng-repeat="panel in ResourceModel.showingPanels | filter:{resource.resourceTypeId:3} " >
			<video id="videoResource{{panel.resource.id}}" control src="{{panel.resource.description}}"></video>
		</li>
	</ul>



</body>

<script type="text/javascript" data-main="<?php echo base_url(); ?>assets/js/app/main.js" src="<?php echo base_url(); ?>assets/js/lib/require.js"></script>

</html>