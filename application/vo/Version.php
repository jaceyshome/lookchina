<?php
require_once "application/vo/ValueObject.php";
class Version extends ValueObject {
	public $id;
	public $name;

	function Version($id) {
		$this->id = (int) $id;
	}
	public static function fromRow($row) {
		$vo = new Version($row->id);
		if (isset($row->id))$vo->id = (int)$row->id;
		if (isset($row->name))$vo->name = $row->name;

		return $vo;
	}
	public static function fromJSON($json) {
		$id = isset($json["id"])?$json["id"]:0;
		$vo = new Version($id);
		if (isset($json["name"]))$vo->name = $json["name"];
		return $vo;
	}
}
?>