<?php
require_once "application/vo/ValueObject.php";
class Dynasty extends ValueObject {
	public $id;
	public $name;
	public $introduction;
	public $editDateTime;
	public $startYear;
	public $endYear;
	public $chinese;
	public $enable;
	public $totalPeriod;

	function Dynasty($id) {
		$this->id = (int) $id;
	}
	public static function fromRow($row) {
		$vo = new Dynasty($row->id);
		if (isset($row->id))$vo->id = (int)$row->id;
		if (isset($row->name))$vo->name = $row->name;
		if (isset($row->introduction))$vo->introduction = $row->introduction;
		if (isset($row->startYear))$vo->startYear = (int)$row->startYear;
		if (isset($row->endYear))$vo->endYear = (int)$row->endYear;
		if (isset($row->chinese))$vo->chinese = (int)$row->chinese;
		if (isset($row->enable))$vo->enable = (int)$row->enable;
		if (isset($row->totalPeriod))$vo->totalPeriod = (int)$row->totalPeriod;

		return $vo;
	}
	public static function fromJSON($json) {
		$id = isset($json["id"])?$json["id"]:0;
		$vo = new Dynasty($id);
		if (isset($json["name"]))$vo->name = $json["name"];
		if (isset($json["introduction"]))$vo->introduction = $json["introduction"];
		if (isset($json["startYear"]))$vo->startYear = $json["startYear"];
		if (isset($json["endYear"]))$vo->endYear = $json["endYear"];
		if (isset($json["chinese"]))$vo->chinese = $json["chinese"];
		if (isset($json["enable"]))$vo->enable = $json["enable"];
		if (isset($json["totalPeriod"]))$vo->enable = $json["totalPeriod"];
		return $vo;
	}
}
?>