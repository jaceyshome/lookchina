<?php
class Options{
	private $options;
	function Options($options = null) {
		if ($options == null)$options = array();
		$this->options = $options;
	}
	public function setOption($key, $value) {
		$options[$key] = $value;
	}
	public function getOption($key, $default = false) {
		$value = $default;
		if(isset($this->options[$key]))$value = $this->options[$key];
		return $value;
	}
}
?>