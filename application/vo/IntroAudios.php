<?php
require_once "application/vo/ValueObject.php";
require_once "application/vo/IntroAudio.php";
class IntroAudios extends ValueObject {
	public $id;
	public $audioId;
	public $audio;
	public $text;
	public $state;

	function IntroAudios($id) {
		$this->id = (int) $id;
	}

	public static function fromJSON($json) {
		$introAudios = array();
		foreach($json as $jsonIntroAudio){
			$introAudio = IntroAudio::fromJSON($jsonIntroAudio);
			array_push($introAudios, $introAudio);
		}
		return $introAudios;
	}
}
?>