<?php
require_once "application/vo/ValueObject.php";
class User extends ValueObject {
	public $id;
	public $userName;
	public $surName;
	public $email;
	public $skypeId;
	public $welinkId;
	public $firstName;
	public $locusId;
	public $scootleId;
	public $currentSchool;
	public $displayInformation;
	public $editDateTime;
	public $schoolclasses;
	public $schoolyears;
	public $language;
	public $dateOfBirth;
	public $agentName;
	public $userTypeId;
	public $contacts;
	public $thumbnailLink;
	public $password;
	public $userTypeName;
	public $isAContact;
	public $userlanguages;
	
	function User($id) {
		$this->id = (int) $id;
	}
	public static function fromRow($row) {
		$vo = new User($row->user_id);
		$datatypes = array(
			'id' => 'int',
			'userName' => 'string',
			'surName' => 'string',
			'email' => 'string',
			'skypeId' => 'string',
			'welinkId' => 'string',
			'firstName' => 'string',
			'locusId' => 'string',
			'currentSchool' => 'string',
			'displayInformation' => 'int',
			'editDateTime' => 'date',
			'scootleId' => 'string',
			'agentName' => 'string',
			'dateOfBirth' => 'date',
			'userTypeId' => 'int',
			'thumbnailLink' => 'string',
			'password' => 'string',
			'userTypeName' => 'string',
		);
		ValueObject::populateBasics($row, $vo, $datatypes);
		if (isset($row->usertype_name))$vo->userTypeName = $row->usertype_name;
		if (isset($row->isAContact))$vo->isAContact = (boolean)$row->isAContact;
		return $vo;
	}
	public static function fromJSON($json) {
		$id = isset($json["id"])?$json["id"]:0;
		$vo = new User($id);
		if (isset($json["userName"]))$vo->userName = $json["userName"];
		if (isset($json["surName"]))$vo->surName = $json["surName"];
		if (isset($json["email"]))$vo->email = $json["email"];
		if (isset($json["skypeId"]))$vo->skypeId = $json["skypeId"];
		if (isset($json["welinkId"]))$vo->welinkId = $json["welinkId"];
		if (isset($json["firstName"]))$vo->firstName = $json["firstName"];
		if (isset($json["locusId"]))$vo->locusId = $json["locusId"];
		if (isset($json["currentSchool"]))$vo->currentSchool = $json["currentSchool"];
		if (isset($json["displayInformation"]))$vo->displayInformation = $json["displayInformation"];
		if (isset($json["scootleId"]))$vo->scootleId = $json["scootleId"];
		if (isset($json["dateOfBirth"]))$vo->dateOfBirth = $json["dateOfBirth"];
		if (isset($json["agentName"]))$vo->agentName = $json["agentName"];
		if (isset($json["userTypeId"]))$vo->userTypeId = $json["userTypeId"];
		if (isset($json["thumbnailLink"]))$vo->thumbnailLink = $json["thumbnailLink"];
		if (isset($json["password"]))$vo->password = $json["password"];
		if (isset($json["isAContact"]))$vo->isAContact = $json["isAContact"];
		if (isset($json["schoolyears"]))$vo->schoolyears = $json["schoolyears"];
		if (isset($json["userlanguages"]))$vo->userlanguages = $json["userlanguages"];
		//if (isset($json["schoolyears"]))$vo->schoolyears = $json["schoolyears"];
		return $vo;
	}
}
?>