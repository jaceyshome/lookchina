<?php
require_once "application/vo/ValueObject.php";
class Category extends ValueObject {
	public $id;
	public $name;
	public $chinese;
	public $iconName;

	function Category($id) {
		$this->id = (int) $id;
	}
	public static function fromRow($row) {
		$vo = new Category($row->id);
		if (isset($row->id))$vo->id = (int)$row->id;
		if (isset($row->name))$vo->name = $row->name;
		if (isset($row->chinese))$vo->chinese = $row->chinese;
		if (isset($row->iconName))$vo->iconName = $row->iconName;

		return $vo;
	}
	public static function fromJSON($json) {
		$id = isset($json["id"])?$json["id"]:0;
		$vo = new Category($id);
		if (isset($json["name"]))$vo->name = $json["name"];
		if (isset($json["chinese"]))$vo->chinese = $json["chinese"];
		if (isset($json["iconName"]))$vo->iconName = $json["iconName"];
		return $vo;
	}
}
?>