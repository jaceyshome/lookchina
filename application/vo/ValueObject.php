<?php
require_once "application/libraries/TimeUtil.php";

class ValueObject {
	public $id;
	
	
	
	function __toString() {
		$className = get_class($this);
		return "[$className: id=$this->id]";
	}
	
	public static function populateBasics($row, $vo, $datatypes = null) {
		$className = get_class($vo);
		$classVars = get_object_vars($vo);
		$prefix = strtolower($className)."_";
		foreach($classVars as $key => $value) {
			$colName = $prefix.$key;
			if (isset($row->$colName)) {
				$vo->$key = $row->$colName;
				if(isset($datatypes[$key])) {
					$type = $datatypes[$key];
					$value = $vo->$key;
					switch ($type) {
						case 'boolean':
						case 'bool':
							$value = (bool) $value;
							break;
						
						case 'integer':
						case 'int':
							$value = (int) $value;
							break;

						case 'float':
							$value = (float) $value;
							break;
							
						case 'string':
							$value = (string) $value;
							break;
						case 'date':
							//convert date from local server time to utc
							//$date = new DateTime($value);
							//$date->setTimezone(new DateTimeZone('UTC'));
							//$value = $date-> format('Y-m-d H:i:s');
							$value = TimeUtil::fromTime($value);
							break;
						default:
							break;
					}
					$vo->$key = $value;
					
				}
			}
		}
	}
	/*public static function cleanJson($json) {

		echo '--Clean JSON--';

		switch (gettype($json)) {
			case "string":
				require_once 'application/libraries/HTMLPurifier.auto.php';
				$config = HTMLPurifier_Config::createDefault();
				$config->set('HTML.TidyLevel', 'heavy');
				$config->set('HTML.Doctype', 'HTML 4.01 Transitional');
				$config->set('Core.Encoding', 'UTF-8');
				$config->set('Cache.DefinitionImpl', null);
				$purifier = new HTMLPurifier($config);
				$json = $purifier->purify($json);
			break;
			case "array":
				foreach($json as $key => $value) {
					$json[$key] = ValueObject::cleanJson($value);
				}
			break;
			default:
			break;
		}

		return $json;
	}*/
}
?>