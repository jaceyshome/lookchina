<?php
require_once "application/vo/ValueObject.php";
class MapConfig extends ValueObject {
	public $id;
	public $period;
	public $dynastyId;
	public $startX;
	public $startY;
	public $startR;
	public $endX;
	public $endY;
	public $endR;
	public $delay;


	function MapConfig($id) {
		$this->id = (int) $id;
	}
	public static function fromRow($row) {
		$vo = new MapConfig($row->id);
		if (isset($row->id))$vo->id = (int)$row->id;
		if (isset($row->period))$vo->period = $row->period;
		if (isset($row->dynastyId))$vo->dynastyId = (int)$row->dynastyId;
		if (isset($row->startX))$vo->startX = (float)$row->startX;
		if (isset($row->startY))$vo->startY = (float)$row->startY;
		if (isset($row->startR))$vo->startR = (float)$row->startR;
		if (isset($row->endX))$vo->endX = (float)$row->endX;
		if (isset($row->endY))$vo->endY = (float)$row->endY;
		if (isset($row->endR))$vo->endR = (float)$row->endR;
		if (isset($row->delay))$vo->delay = (int)$row->delay;

		return $vo;
	}
	public static function fromJSON($json) {
		$id = isset($json["id"])?$json["id"]:0;
		$vo = new MapConfig($id);
		if (isset($json["period"]))$vo->period = $json["period"];
		if (isset($json["dynastyId"]))$vo->dynastyId = $json["dynastyId"];
		if (isset($json["startX"]))$vo->startX = $json["startX"];
		if (isset($json["startY"]))$vo->startX = $json["startY"];
		if (isset($json["startR"]))$vo->startX = $json["startR"];
		if (isset($json["endX"]))$vo->startX = $json["endX"];
		if (isset($json["endY"]))$vo->startX = $json["endY"];
		if (isset($json["endR"]))$vo->startX = $json["endR"];
		if (isset($json["delay"]))$vo->startX = $json["delay"];
		return $vo;
	}
}
?>