<?php
require_once "application/vo/ValueObject.php";
class Resource extends ValueObject {
	public $id;
	public $name;
	public $description;
	public $introduction;
	public $editDateTime;
	public $resourceCategoryId;
	public $thumbUrl;
	public $dynastyId;
	public $x;
	public $y;
	public $z;
	public $resourceTypeId;
	public $iconName;

	function Resource($id) {
		$this->id = (int) $id;
	}
	public static function fromRow($row) {
		$vo = new Resource($row->id);
		if (isset($row->id))$vo->id = (int)$row->id;
		if (isset($row->name))$vo->name = $row->name;
		if (isset($row->description))$vo->description = $row->description;
		if (isset($row->introduction))$vo->introduction = $row->introduction;
		if (isset($row->editDateTime))$vo->editDateTime = TimeUtil::fromTime($row->editDateTime);
		if (isset($row->resourceCategoryId))$vo->resourceCategoryId = (int)$row->resourceCategoryId;
		if (isset($row->thumbUrl))$vo->thumbUrl = $row->thumbUrl;
		if (isset($row->dynastyId))$vo->dynastyId = (int)$row->dynastyId;
		if (isset($row->x))$vo->x = (float)$row->x;
		if (isset($row->y))$vo->y = (float)$row->y;
		if (isset($row->z))$vo->z = (float)$row->z;
		if (isset($row->resourceTypeId))$vo->resourceTypeId = (int)$row->resourceTypeId;
		if (isset($row->iconName))$vo->iconName = $row->iconName;

		return $vo;
	}
	public static function fromJSON($json) {
		$id = isset($json["id"])?$json["id"]:0;
		$vo = new Resource($id);
		if (isset($json["name"]))$vo->name = $json["name"];
		if (isset($json["description"]))$vo->description = $json["description"];
		if (isset($json["introduction"]))$vo->introduction = $json["introduction"];
		if (isset($json["editDateTime"]))$vo->editDateTime = $json["editDateTime"];
		if (isset($json["resourceCategoryId"]))$vo->resourceCategoryId = $json["resourceCategoryId"];
		if (isset($json["thumbUrl"]))$vo->thumbUrl = $json["thumbUrl"];
		if (isset($json["dynastyId"]))$vo->dynastyId = $json["dynastyId"];
		if (isset($json["x"]))$vo->x = $json["x"];
		if (isset($json["y"]))$vo->y = $json["y"];
		if (isset($json["z"]))$vo->z = $json["z"];
		if (isset($json["resourceTypeId"]))$vo->resourceTypeId = $json["resourceTypeId"];
		if (isset($json["iconName"]))$vo->iconName = $json["iconName"];
		return $vo;
	}
}
?>