<?php
require_once "application/vo/ValueObject.php";
class ResourceType extends ValueObject {
	public $id;
	public $name;
	public $chinese;

	function ResourceType($id) {
		$this->id = (int) $id;
	}
	public static function fromRow($row) {
		$vo = new ResourceType($row->id);
		if (isset($row->id))$vo->id = (int)$row->id;
		if (isset($row->name))$vo->name = $row->name;

		return $vo;
	}
	public static function fromJSON($json) {
		$id = isset($json["id"])?$json["id"]:0;
		$vo = new ResourceType($id);
		if (isset($json["name"]))$vo->name = $json["name"];
		return $vo;
	}
}
?>