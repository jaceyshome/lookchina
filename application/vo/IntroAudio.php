<?php
require_once "application/vo/ValueObject.php";
class IntroAudio extends ValueObject {
	public $id;
	public $audioId;
	public $audio;
	public $text;
	public $state;

	function IntroAudio($id) {
		$this->id = (int) $id;
	}

	public static function fromJSON($json) {
		$id = isset($json["id"])?$json["id"]:0;
		$vo = new IntroAudio($id);
		if (isset($json["audioId"]))$vo->audioId = $json["audioId"];
		if (isset($json["audio"]))$vo->audio = $json["audio"];
		if (isset($json["text"]))$vo->text = $json["text"];
		if (isset($json["state"]))$vo->state = $json["state"];
		return $vo;
	}
}
?>