 define ['angular', 'angular_resource'], ->
    module = angular.module 'app', ['ngResource']
    module.config ['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider)=>
    	$locationProvider.html5Mode false
    ]
    return module
