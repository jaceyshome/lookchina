define ['angular', 'appmodule', 'angular_resource'], =>
	appModule = angular.module 'app'

	appModule.factory "RemoteResourceDynastiesService", ["$resource",  ($resource) ->
		$resource site_url+'api/dynasties/:dynastyId', {},
			list:{ method:	"GET", params:{}, isArray:true 	}
	]

	appModule.factory "RemoteResourceTypesService", ["$resource", ($resource) ->
		$resource site_url+'api/resourcetypes/:resourceTypeId', {},
			list:{ method:	"GET", params:{}, isArray:true 	}
	]

	appModule.factory "RemoteResourceCategoriesService", ["$resource", ($resource) ->
		$resource site_url+'api/resourcecategories/:categoryId', {},
			list:{ method:	"GET", params:{}, isArray:true 	}
	]

	appModule.factory "RemoteResourceVersionService", ["$resource", ($resource) ->
		$resource site_url+'api/latestversion/', {},
			get:{ method:	"GET", params:{}, isArray:false 	}
	]

	appModule.factory "RemoteIntroAudioService", ["$resource", ($resource) ->
		$resource site_url+'api/txttospeech/', {},
			save:{ method:	"POST", params:{}, isArray:true 	}
	]