define ['angular', 'appmodule', 'angular_resource'], =>
	appModule = angular.module 'app'
	appModule.factory 'LocalResourceDynastiesService', ['ResourceModel', "$resource" ,(ResourceModel,$resource ) =>
		{
			load:(callback)->
				ResourceModel.dynasties = JSON.parse window.localStorage.getItem("ResourceDynasties")

			remove:(callback)->
				
					window.localStorage.removeItem('ResourceDynasties')

			update:(callback)->
				
					window.localStorage.removeItem('ResourceDynasties')
					window.localStorage.setItem('ResourceDynasties', JSON.stringify(ResourceModel.dynasties))

			save:(callback)->
				
					window.localStorage.setItem('ResourceDynasties', JSON.stringify(ResourceModel.dynasties))
		}
	]

	appModule.factory 'LocalResourceTypesService', ['ResourceModel' ,(ResourceModel ) =>
		{
			load:(callback)->
				ResourceModel.resourceTypes = JSON.parse window.localStorage.getItem "ResourceTypes"

			remove:(callback)->
				window.localStorage.removeItem 'ResourceTypes'

			update:(callback)->
				window.localStorage.removeItem 'ResourceTypes'
				window.localStorage.setItem 'ResourceTypes', JSON.stringify(ResourceModel.resourceTypes)

			save:(callback)->
				window.localStorage.setItem 'ResourceTypes', JSON.stringify(ResourceModel.resourceTypes)
		}
	]

	appModule.factory 'LocalResourceCategoriesService', ['ResourceModel' ,(ResourceModel) =>
		{
			load:(callback)->
				ResourceModel.resourceCategories = JSON.parse window.localStorage.getItem "ResourceCategories"
				callback()

			remove:(callback)->
				window.localStorage.removeItem 'ResourceCategories'

			update:(callback)->
				window.localStorage.removeItem 'ResourceCategories'
				window.localStorage.setItem 'ResourceCategories', JSON.stringify(ResourceModel.resourceCategories)

			save:(callback)->
				window.localStorage.setItem 'ResourceCategories', JSON.stringify(ResourceModel.resourceCategories)
		}
	]

	appModule.factory 'LocalStorageService', ['ResourceModel' ,(ResourceModel) =>
		{

			clear:(callback)->
				window.localStorage.clear()

			updateResourceVersion:()->
				window.localStorage.removeItem 'resourceVersion'
				window.localStorage.setItem 'resourceVersion', JSON.stringify(ResourceModel.version)

			getResourceVersion:()->
				ResourceModel.version = JSON.parse window.localStorage.getItem "resourceVersion"
		}
	]
