// Generated by CoffeeScript 1.6.3
(function() {
  var _this = this;

  define(['angular', 'appmodule', 'angular_resource'], function() {
    var appModule;
    appModule = angular.module('app');
    return appModule.factory('PreloadDataService', [
      'SystemParameters', 'ResourceModel', 'CSV', 'RemoteResourceDynastiesService', 'RemoteResourceVersionService', 'RemoteResourceTypesService', 'RemoteResourceCategoriesService', 'RemoteIntroAudioService', 'LocalResourceDynastiesService', 'LocalResourceTypesService', 'LocalResourceCategoriesService', 'LocalStorageService', function(SystemParameters, ResourceModel, CSV, RemoteResourceDynastiesService, RemoteResourceVersionService, RemoteResourceTypesService, RemoteResourceCategoriesService, RemoteIntroAudioService, LocalResourceDynastiesService, LocalResourceTypesService, LocalResourceCategoriesService, LocalStorageService) {
        return {
          preloadData: function($scope, callback) {
            var checkAllDynastiesIntroAudioLoadingState, checkAllPreload, loadCategoryIconComplete, loadDynastiesIntroAudios, loadDynastyIntroAudio, loadPercentage, loadResourceCategoriesIcons,
              _this = this;
            loadPercentage = 0;
            SystemParameters.preloadState = SystemParameters.START;
            checkAllPreload = function(percentage) {
              loadPercentage += percentage;
              if (loadPercentage >= 100) {
                return callback();
              }
            };
            LocalStorageService.getResourceVersion();
            loadCategoryIconComplete = function() {
              return checkAllPreload(1);
            };
            loadResourceCategoriesIcons = function() {
              var category, image, _i, _len, _ref, _results;
              if (!ResourceModel.resourceCategoryIcons) {
                ResourceModel.resourceCategoryIcons = [];
                _ref = ResourceModel.resourceCategories;
                _results = [];
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                  category = _ref[_i];
                  if (category.iconName) {
                    image = {};
                    image.data = new Image();
                    image.data.onload = function() {
                      if (this.complete) {
                        return loadCategoryIconComplete();
                      }
                    };
                    image.data.src = 'assets/images/' + category.iconName + '.png';
                    _results.push(ResourceModel.resourceCategoryIcons[category.iconName] = image);
                  } else {
                    _results.push(void 0);
                  }
                }
                return _results;
              }
            };
            checkAllDynastiesIntroAudioLoadingState = function(dynasties, callback) {
              var allCompleted, dynasty, _i, _len;
              allCompleted = true;
              for (_i = 0, _len = dynasties.length; _i < _len; _i++) {
                dynasty = dynasties[_i];
                if (dynasty.introAudiosState !== SystemParameters.introAudioState.LOADED) {
                  allCompleted = false;
                  break;
                }
              }
              if (allCompleted) {
                return callback();
              }
            };
            loadDynastyIntroAudio = function(dynasty, callback) {
              return RemoteIntroAudioService.save({}, dynasty.introAudios, (function(introAudios) {
                var introAudio, _i, _len;
                for (_i = 0, _len = introAudios.length; _i < _len; _i++) {
                  introAudio = introAudios[_i];
                  introAudio.state = SystemParameters.introAudioState.LOADED;
                }
                dynasty.introAudiosState = SystemParameters.introAudioState.LOADED;
                dynasty.introAudios = introAudios;
                return checkAllDynastiesIntroAudioLoadingState(ResourceModel.dynasties, callback);
              }));
            };
            loadDynastiesIntroAudios = function(dynasties, callback) {
              var data, dynasty, index, introAudios, introduction, sentence, sentences, subSentence, subSentences, _i, _j, _k, _len, _len1, _len2, _results;
              _results = [];
              for (_i = 0, _len = dynasties.length; _i < _len; _i++) {
                dynasty = dynasties[_i];
                if (!dynasty.introduction || dynasty.introduction === "") {
                  dynasty.introAudios = void 0;
                  dynasty.introAudiosState = SystemParameters.introAudioState.LOADED;
                  _results.push(checkAllDynastiesIntroAudioLoadingState(dynasties, callback));
                } else {
                  dynasty.introAudiosState = SystemParameters.introAudioState.INIT;
                  introduction = dynasty.introduction;
                  sentences = introduction.split(/\b[\.\!\?][\s]{1,2}(?=[A-Z])/);
                  index = 0;
                  introAudios = [];
                  for (_j = 0, _len1 = sentences.length; _j < _len1; _j++) {
                    sentence = sentences[_j];
                    if (sentence.length > 100) {
                      subSentences = sentence.split(',');
                      for (_k = 0, _len2 = subSentences.length; _k < _len2; _k++) {
                        subSentence = subSentences[_k];
                        data = {
                          audioId: dynasty.name + '_' + index,
                          id: index,
                          audio: 'assets/data/audios/' + dynasty.name + '_' + index + '.mp3',
                          text: subSentence,
                          state: SystemParameters.introAudioState.INIT
                        };
                        introAudios.push(data);
                        index++;
                      }
                    } else {
                      data = {
                        audioId: dynasty.name + '_' + index,
                        id: index,
                        audio: 'assets/data/audios/' + dynasty.name + '_' + index + '.mp3',
                        text: sentence,
                        state: SystemParameters.introAudioState.INIT
                      };
                      introAudios.push(data);
                      index++;
                    }
                  }
                  dynasty.introAudios = introAudios;
                  _results.push(loadDynastyIntroAudio(dynasty, callback));
                }
              }
              return _results;
            };
            RemoteResourceVersionService.get({}, function(data) {
              if (ResourceModel.version && ResourceModel.version.name === data.name) {
                console.log("load data from local storage");
                LocalResourceDynastiesService.load();
                LocalResourceTypesService.load();
                LocalResourceCategoriesService.load(loadResourceCategoriesIcons);
                return checkAllPreload(7);
              } else {
                checkAllPreload(1);
                console.log("load data from remote server");
                ResourceModel.version = data;
                LocalStorageService.updateResourceVersion();
                RemoteResourceDynastiesService.query({}, function(data) {
                  ResourceModel.dynasties = data;
                  return loadDynastiesIntroAudios(ResourceModel.dynasties, function() {
                    LocalResourceDynastiesService.save();
                    return checkAllPreload(9);
                  });
                });
                RemoteResourceCategoriesService.query({}, function(data) {
                  ResourceModel.resourceCategories = data;
                  loadResourceCategoriesIcons();
                  LocalResourceCategoriesService.save();
                  return checkAllPreload(1);
                });
                return RemoteResourceTypesService.query({}, function(data) {
                  ResourceModel.resourceTypes = data;
                  LocalResourceTypesService.save();
                  return checkAllPreload(1);
                });
              }
            });
            return CSV.loadImageInfos(function(data) {
              var loadImages, loadImagesComplete;
              loadImages = function(images) {
                var checkTotalLoadedImages, image, percentageForEachImage, totalIndex, _i, _len;
                totalIndex = 0;
                if (SystemParameters.images === void 0) {
                  percentageForEachImage = 90 / images.length;
                  for (_i = 0, _len = images.length; _i < _len; _i++) {
                    image = images[_i];
                    image.data = new Image();
                    image.x = image.startX;
                    image.y = image.startY;
                    image.a = 1;
                    image.data.onload = function() {
                      if (this.complete) {
                        totalIndex++;
                        checkTotalLoadedImages();
                        return checkAllPreload(percentageForEachImage);
                      }
                    };
                    image.data.src = 'assets/images/' + image.name;
                  }
                  return checkTotalLoadedImages = function() {
                    if (totalIndex >= images.length - 1) {
                      return loadImagesComplete(images);
                    }
                  };
                }
              };
              loadImagesComplete = function(images) {
                return SystemParameters.images = images;
              };
              return loadImages(data);
            });
          }
        };
      }
    ]);
  });

}).call(this);

/*
//@ sourceMappingURL=preloaddata.map
*/
