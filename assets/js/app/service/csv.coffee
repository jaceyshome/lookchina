define ['angular', 'appmodule', 'angular_resource', 'csv2json'], =>
	appModule = angular.module 'app'
	appModule.factory 'CSV', ['$http', ($http) =>
		return {
		loadImageInfos: (callback) =>
			url = 'assets/data/csv/images.csv'
			$http.get(url,
				transformResponse:(data) =>
					json = csvjson.csv2json(data, {
						delim: ",",
						textdelim: "\""
					})
					return json).success (data, status) =>
						callback(data.rows)
		}
	]