define ['angular', 'appmodule', 'angular_resource'], =>
	appModule = angular.module 'app'
	appModule.factory 'PreloadDataService', [
		'SystemParameters',
		'ResourceModel',
		'CSV' ,
		'RemoteResourceDynastiesService',
		'RemoteResourceVersionService',
		'RemoteResourceTypesService',
		'RemoteResourceCategoriesService',
		'RemoteIntroAudioService',
		'LocalResourceDynastiesService',
		'LocalResourceTypesService',
		'LocalResourceCategoriesService',
		'LocalStorageService',
		(SystemParameters,ResourceModel, CSV, RemoteResourceDynastiesService, RemoteResourceVersionService, RemoteResourceTypesService, RemoteResourceCategoriesService, RemoteIntroAudioService,LocalResourceDynastiesService,LocalResourceTypesService,LocalResourceCategoriesService, LocalStorageService) =>
			{

				preloadData : ($scope, callback)->
					loadPercentage = 0
					SystemParameters.preloadState = SystemParameters.START

					checkAllPreload = (percentage)->
						loadPercentage += percentage
						if loadPercentage >= 100
							callback()

#					LocalStorageService.clear()
					LocalStorageService.getResourceVersion()

					loadCategoryIconComplete = ()->
#						console.log ResourceModel.resourceCategoryIcons
						checkAllPreload(1)

					loadResourceCategoriesIcons = ()->
#						checkAllPreload(5)
						if !ResourceModel.resourceCategoryIcons
							ResourceModel.resourceCategoryIcons = []
							for category in ResourceModel.resourceCategories
								if category.iconName
									image = {}
									image.data = new Image()
									image.data.onload = ()->
										if this.complete
											loadCategoryIconComplete()
									image.data.src = 'assets/images/' + category.iconName + '.png'
									ResourceModel.resourceCategoryIcons[category.iconName] = image

					checkAllDynastiesIntroAudioLoadingState = (dynasties,callback)->
						allCompleted = true;

						for dynasty in dynasties

							if dynasty.introAudiosState != SystemParameters.introAudioState.LOADED
								allCompleted = false
								break

						if allCompleted
							callback()

					loadDynastyIntroAudio = (dynasty, callback)->
						RemoteIntroAudioService.save {}, dynasty.introAudios, ((introAudios)->
							for introAudio in introAudios
								introAudio.state = SystemParameters.introAudioState.LOADED
							dynasty.introAudiosState = SystemParameters.introAudioState.LOADED
							dynasty.introAudios = introAudios
							checkAllDynastiesIntroAudioLoadingState(ResourceModel.dynasties,callback)
						)

					loadDynastiesIntroAudios = (dynasties,callback)->
						for dynasty in dynasties
							if (!dynasty.introduction or dynasty.introduction == "")
								dynasty.introAudios = undefined
								dynasty.introAudiosState = SystemParameters.introAudioState.LOADED
								checkAllDynastiesIntroAudioLoadingState(dynasties,callback)
							else
								dynasty.introAudiosState = SystemParameters.introAudioState.INIT
								introduction = dynasty.introduction
								sentences = introduction.split(/\b[\.\!\?][\s]{1,2}(?=[A-Z])/)

								index = 0
								introAudios = []
								for sentence in sentences
									if sentence.length > 100
										subSentences = sentence.split(',')
										for subSentence in subSentences
											data =
												audioId:dynasty.name + '_' + index
												id:index
												audio:'assets/data/audios/'+ dynasty.name + '_' + index + '.mp3'
												text:subSentence
												state:SystemParameters.introAudioState.INIT
											introAudios.push(data)
											index++;
									else
										data =
											audioId:dynasty.name + '_' + index
											id:index
											audio:'assets/data/audios/'+ dynasty.name + '_' + index + '.mp3'
											text:sentence
											state:SystemParameters.introAudioState.INIT
										introAudios.push(data)
										index++;
								dynasty.introAudios = introAudios

								loadDynastyIntroAudio(dynasty,callback)

					RemoteResourceVersionService.get {}, (data) =>
#						 check browser support storge : typeof Storage != undefined
#						console.log "current resource version:", ResourceModel.version.name
						if ResourceModel.version && ResourceModel.version.name == data.name

							console.log "load data from local storage"

							LocalResourceDynastiesService.load()
							LocalResourceTypesService.load()
							LocalResourceCategoriesService.load(loadResourceCategoriesIcons)

							checkAllPreload(7)
						else
							checkAllPreload(1)
							console.log "load data from remote server"
							ResourceModel.version = data
							LocalStorageService.updateResourceVersion()

							RemoteResourceDynastiesService.query {}, (data) =>
								ResourceModel.dynasties = data
								loadDynastiesIntroAudios(ResourceModel.dynasties, ()->
#									console.log ResourceModel.dynasties
									LocalResourceDynastiesService.save()
									checkAllPreload(9)
								)

							RemoteResourceCategoriesService.query {}, (data)=>
								ResourceModel.resourceCategories = data
								loadResourceCategoriesIcons()
								LocalResourceCategoriesService.save()
								checkAllPreload(1)

							RemoteResourceTypesService.query {}, (data) =>
								ResourceModel.resourceTypes = data
								LocalResourceTypesService.save()
								checkAllPreload(1)

					CSV.loadImageInfos((data)->

						loadImages = (images)->
							totalIndex = 0
							if(SystemParameters.images == undefined )
								percentageForEachImage = 90 / images.length
								for image in images
									image.data = new Image()
									image.x = image.startX
									image.y = image.startY
									image.a = 1
									image.data.onload = ()->
										if this.complete
											totalIndex++
											checkTotalLoadedImages()
											checkAllPreload(percentageForEachImage)
									image.data.src = 'assets/images/' + image.name
								checkTotalLoadedImages = ()->
									if(totalIndex >= images.length - 1)
										loadImagesComplete(images)

						loadImagesComplete = (images)->
							SystemParameters.images = images


						loadImages(data)

					)




			}
	]