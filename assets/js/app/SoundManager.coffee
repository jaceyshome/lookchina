define ["sound", 'angular', 'appmodule'] , () ->

	class SoundManager
	
		@initialised = false
		@autoplay = true

		@currentSounds = []
		
		@player
		@soundManagerReady
		@isFinish
		
		@init:(bgEqData, bgSoundOn, callback) ->
			soundManager.setup
				url:document.URL + "/assets/swf"
				flashVersion: 9
#				preferFlash: false
				debugMode: false
				waitForWindowLoad: true
				useConsole: true
				onready: ->
					SoundManager.soundManagerReady = true
					console.log "sound ready"
					callback() if callback
# 	play empty sound

					tmpIntroAudio = {
						audioId:"tmpIntroAudio"
						audio:"assets/audios/tmpIntroAudio.mp3"
					}
					SoundManager.playIntroAudio(tmpIntroAudio)

					if bgSoundOn
						SoundManager.playBgSound(bgEqData)



		@playBgSound:(callback)->
#			return undefined
			if SoundManager.soundManagerReady

				player = soundManager.createSound(
					{
						id:"bgAudio"
						url: 'assets/audios/demo.mp3'
						autoPlay: true
						autoLoad: true
						stream: true
						useWaveformData: false
						useEQData: true
						volume:2
						stream: true
						whileplaying:()->
							callback(this.eqData)
#							console.log eqData
#						onfinish: ()->
#							#console.log "onFinishPlaying callback", callback
##							callback?()
					}
				)

				player.play({
					autoPlay: true
					autoLoad: true
					stream: true
					useWaveformData: false
					useEQData: true
				})
		@stopIntroAudio:(introAudio)->
			currentIntroAudio = soundManager.getSoundById(introAudio.audioId)
			soundManager.stop(introAudio.audioId)

		@playIntroAudio:( introAudio, callback, onFinishCallback)->
			if SoundManager.soundManagerReady
				console.log "play sound", introAudio.audioId
				player = soundManager.createSound(
					{
						id:introAudio.audioId
						url: introAudio.audio
						autoPlay: true
						autoLoad: true
						useWaveformData: false
						useEQData: true
						volume:80
						onfinish:onFinishCallback
						whileplaying:()->
							callback(this.eqData)
	#							console.log eqData
	#						onfinish: ()->
	#							#console.log "onFinishPlaying callback", callback
	##							callback?()
					}
				)
#stop sound before calling play() to  trigger a sound numerous times
				SoundManager.stopIntroAudio(introAudio)
				player.play()
