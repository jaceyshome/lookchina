define ['angular', 'appmodule', 'angular_resource'], =>
	appModule = angular.module 'app'
	appModule.factory 'ResourceModel', [() =>
		{
			version:undefined
			resourceTypes:undefined
			resourceCategories:undefined
			resourceCategoryIcons:undefined
			dynasties:undefined

			currentDynasty:undefined
			currentDynastyId:undefined
			currentDynastyPeriod:undefined

			currentMap:undefined
			newMap:undefined

			showingPanels:[]
			showingMaximizedPanel:undefined

			currentIntroAudio:{
				id:undefined
				audioId:undefined
				audio:undefined
				text:undefined
				state:undefined
			}
			drawingIntroText:undefined
		}
	]