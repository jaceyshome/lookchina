define ['angular', 'appmodule', 'angular_resource'], =>
	appModule = angular.module 'app'
	appModule.factory 'EarthModel', [() =>
		{
			camera:undefined
			scene:undefined
			renderer:undefined
			canvas:undefined

			light: undefined
			earth: undefined
			clouds: undefined
			stars:undefined
		}
	]