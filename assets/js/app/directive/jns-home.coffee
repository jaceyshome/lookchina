define ['angular', 'appmodule', 'jquery'], ->

	appModule = angular.module 'app'
	appModule.directive 'jnsHome', () ->
		($scope, $element, $attrs) ->

			loadingEle =  $($element).find($(".percentage"))

			loadImage = (imageId, percentage)->
				img = $(imageId)[0]
				img.onload = ()->
					if this.complete
						$scope.loadingFrontPageProcess += percentage
						loadingEle.html("Loading...")
						loadingEle.css({"display" : "block"})
						checkAllHomePages()

			checkAllHomePages = ()->
				console.log $scope.loadingFrontPageProcess
				if ($scope.loadingFrontPageProcess >= 100)
					animateStart()
					loadingEle.css({"display" : "none"})

			animateStart = ()->
				titleIn()

			titleIn = ()->
				$('.title').fadeIn("slow",()->
					fontImageIn()
				)

			fontImageIn = ()->
				$(".frontImage").animate({
						top:'50px'
					}, "slow", ()->
							seaIn()
				)

			seaIn = ()->
				$('.seaImage').fadeIn("slow",()->
						seaBgIn()
				)

			seaBgIn = ()->
				$('.seaBgImage').fadeIn("fast",()->
						cloudReset(".cloud1", 64000)
						cloudReset(".cloud2", 54000)
						cloudReset(".cloud3", 34000)
						sunReset()
				)

			cloudReset = (element, duration)->
				topMax = 330
				topMin = 30
				top = (Math.floor((Math.random()*topMax)+topMin)).toString() + "px"
				$(element).css({"top" : top, "left" : "-200px"})
				cloudIn(element, duration)

			cloudIn = (element, duration)->
				$(element).animate({
					left: "1200px"
				}, duration, ()->
					cloudReset(element, duration)
				)

			sunReset = ()->
				duration = 100000
				top = "150px"
				left = "-200px"
				$('.sun').css({"top" : top, "left" : left})
				sunIn(duration)

			sunIn = (duration)->
				$('.sun').animate({
					left: "1300px",
					top:"-50px"
				}, duration, ()->
					sunReset()
				)

			if($scope.loadingFrontPageProcess >= 100)
				animateStart()
				loadingEle.css({"display" : "none"})
			else
				loadingEle.css({"display" : "block"})
				$scope.loadingFrontPageProcess = 0
				loadImage(".seaBgImage", 15)
				loadImage(".seaImage", 15)
				loadImage(".cloud1", 10)
				loadImage(".cloud2", 12)
				loadImage(".sun", 13)
				loadImage(".cloud3", 15)
				loadImage(".frontImage", 20)


			return false

