define ['angular', 'appmodule', 'jquery'], ->

	appModule = angular.module 'app'
	appModule.directive 'jnsPopupImage', () ->
		restrict: "A"
		($scope, $element, $attrs) ->
			imageUrl = $attrs.jnsPopupImage
			$element.click( ()->
				showCurrentImage(imageUrl)
			)

			showCurrentImage = (imageUrl)->

				if $scope.currentImage == imageUrl
					$('.popupImage').fadeIn()
					return

				$scope.currentImage = imageUrl

				img = $(".popupImage").find($("img"))[0]
				img.onload = ()->
					if this.complete
						$('.popupImage').fadeIn()
						offset = (this.clientHeight + 6).toString() + "px"
						$(".popupImage").find($("a")).css({"bottom": offset})

				img.src = imageUrl

