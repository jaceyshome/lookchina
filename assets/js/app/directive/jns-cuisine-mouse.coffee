define ['angular', 'appmodule', 'jquery'], ->

	appModule = angular.module 'app'
	appModule.directive 'jnsCuisineMouse', () ->
		($scope, element, attrs) ->

			colors = Array()
			colors[0] = "#22f07e"
			colors[1] = "#f03f2c"
			colors[2] = "#f0ee32"
			colors[3] = "#f07a1b"
			colors[4] = "#22f07e"
			colors[5] = "#3cccf0"
			colors[6] = "#f06bb0"

			color = colors[Math.floor((Math.random()*6)+0)]

			$(element).mouseover( ()->
				$(element).css({"background-color" : color})
			)

			$(element).mouseout( ()->
				$(element).css({"background-color" : "#ffffff"})

			)