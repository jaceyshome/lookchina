define [
	'SoundManager',
	'angular',
	'appmodule',
	'browserdetect'
	], (SoundManager)=>
	appModule = angular.module 'app'

	appModule.controller 'MainScreenCtrl', [
		'$scope',
		'EVENT',
		'CSV',
		'SystemParameters',
		'ResourceModel',
		'PreloadDataService',
		'DynastyIntroAudioControl',
		'EarthControl',
		'NavigationPanelControl',
		'TimelinePanelControl',
		'CategoryPanelControl',
		'DynastyMapControl',
		'MapAreaPanelControl',
		'ShowingPanelsControl',
		'MaximizedPanelControl',
		($scope, EVENT, CSV,SystemParameters, ResourceModel,PreloadDataService, DynastyIntroAudioControl,EarthControl,NavigationPanelControl,TimelinePanelControl,CategoryPanelControl,DynastyMapControl,MapAreaPanelControl,ShowingPanelsControl,MaximizedPanelControl) =>

	#	--------------- propreties  --------------------
			$scope.context = undefined
			$scope.canvas = undefined
			$scope.mainScreen = null
			$scope.mainScreen = angular.element("#mainScreen")
			$scope.earthContainer = angular.element("#earthContainer")
			$scope.loadDataComplete = false
			$scope.currentDynasty = ResourceModel.currentDynasty
			$scope.showingVideoResources = []
			$scope.soudManagerReady = false
			$scope.eqData = undefined
			$scope.bgSoundOn = false
			$scope.bg3DearthOn = true
			$scope.alphaCanvas = false
			$scope.betaCanvas = false

	#	---------------- main functions ------------------

			$scope.eqData = []

			SoundManager.init((eqData)->
				$scope.eqData = eqData
			, $scope.bgSoundOn)

			$scope.$watch("earthContainer", (e)->
				if $scope.bg3DearthOn
					if(e != undefined)
						$scope.earthContainer = document.getElementById('earthContainer')
						EarthControl.init($scope)
			)

			$scope.$watch("mainScreen", (e)->
				if(e != undefined && $scope.earthContainer )
					$scope.canvas = document.getElementById('mainScreen')
					if($scope.canvas)
						SystemParameters.canvas = $scope.canvas
						registerCanvasEvent()
						PreloadDataService.preloadData($scope,
						()->
							init()
						)

			)


			drawTemplateBG = ->
				if(SystemParameters.screenBgTmp == undefined )
					SystemParameters.screenBgTmp = $scope.getImageByName('screenBgTmp')
				$scope.drawImage(SystemParameters.screenBgTmp)


			onMouseMove = (e)->
				hitspot = SystemParameters.mapAreaPanel.hitspot

				data = {
					mapCircle:{
						x:hitspot.x + hitspot.width / 2
						y:hitspot.y + hitspot.height
						r:hitspot.width / 2
					}
					cursor:{
						x: e.x
						y: e.y
					}
				}

				$scope.$emit(EVENT.UPDATE_POINTER_POSITION,data)

			registerCanvasEvent = ->

				$scope.canvas.onmousedown = (e)->
					if !SystemParameters.onPlayingAnimation
						e.preventDefault()
						TimelinePanelControl.handleMouseDown(e, $scope)

				$scope.canvas.onmousemove = (e)->
					if !SystemParameters.onPlayingAnimation
						e.preventDefault()
						onMouseMove(e)
						TimelinePanelControl.handleMouseMove(e, $scope)
#						MapAreaPanelControl.handleMouseMove(e,$scope)

				$scope.canvas.onmouseup = (e)->
					if !SystemParameters.onPlayingAnimation
						e.preventDefault()
						CategoryPanelControl.handleMouseUp(e, $scope)
						TimelinePanelControl.handleMouseUp(e, $scope)
						NavigationPanelControl.handleMouseUp(e,$scope)
						MapAreaPanelControl.handleMouseUp(e,$scope)


			init = ()->
				window.onload()
				$scope.ResourceModel = ResourceModel
#				$scope.$apply($scope.ResourceModel)

				undefined

			window.requestAnimFrame = (callback)->
				window.requestAnimationFrame ||
				window.webkitRequestAnimationFrame ||
				window.mozRequestAnimationFrame ||
				window.oRequestAnimationFrame ||
				window.msRequestAnimationFrame ||
				(callback)->
					window.setTimeout(callback, 1000 / 24)

			window.onload = ()->
				onload()
#if testing false

				animate()

			onload = ()->
				$scope.alphaCanvas = document.getElementById('alphaCanvas')
				$scope.alphaContext = $scope.alphaCanvas.getContext('2d')
				$scope.betaCanvas = document.getElementById('betaCanvas')
				$scope.betaContext = $scope.betaCanvas.getContext('2d')

				$scope.context = $scope.canvas.getContext('2d')
				SystemParameters.context = $scope.context

#if testing true
#				update()

			animate = ()->
				update()
				TWEEN.update()
				window.requestAnimationFrame(animate)


			update = ()->
				$scope.context.clearRect(0,0,$scope.canvas.width,$scope.canvas.height)
				#draw objects
	#					drawScreenBgTmp()
				if $scope.bg3DearthOn
					EarthControl.update()
					EarthControl.draw()
#				else
#					drawTemplateBG()

				DynastyMapControl.update($scope)
				DynastyMapControl.draw($scope)
				NavigationPanelControl.update($scope)
				NavigationPanelControl.draw($scope)
				CategoryPanelControl.draw($scope)
				TimelinePanelControl.draw($scope)
				MapAreaPanelControl.draw($scope)
				DynastyIntroAudioControl.draw($scope)

	# ------------------ watchers -----------------------------------------
	#				TODO	watcher ResourceModel and systemParameters changes


	# ------------------ helper functions ---------------------------
			$scope.clearAlphaContext = ()->
				if !$scope.alphaContext then return
				$scope.alphaContext.clearRect(0,0,$scope.alphaCanvas.width,$scope.alphaCanvas.height)

			$scope.clearBetaContext = ()->
				if !$scope.betaContext then return
				$scope.betaContext.clearRect(0,0,$scope.betaCanvas.width,$scope.betaCanvas.height)

			$scope.getColor = (name, alpha)->
				colorCode = SystemParameters.colors[name]
				if !alpha then alpha = 1
				color = "rgba(" + colorCode + "," + alpha + ")"
				return color

			$scope.getFont = (text)->
				if text.fontFamily then fontFamily = text.fontFamily else fontFamily = "DEFAULT"
				if text.fontSize then fontSize = text.fontSize else fontSize = "MIDDLE"
				if text.fontWeight then fontWeight = text.fontWeight else fontWeight = "normal"

				fontFamily = SystemParameters.fontFamily[fontFamily]
				fontSize = SystemParameters.fontSize[fontSize]

	#					console.log  fontWeight + ' ' + fontSize + ' ' + fontFamily
				return fontWeight + ' ' + fontSize + ' ' + fontFamily

			$scope.loadImageData = (image, callback)->
				return
				if !image.data
					image.data = new Image()
					image.data.onload = ()->
						if this.complete
							callback()
							return image
					image.data.src = image.url

			$scope.getImageByName = (name)->
				for image in SystemParameters.images
	#						console.log image.name.split('.')[0]
					if(image.name.split('.')[0] == name)
						return image



#------------------------------------- draw functions ----------------------------------
			$scope.drawParagraph = (text)->
				ctx = $scope.context
				words = text.text.split(' ')
				if text.lineHeight then lineHeight = text.lineHeight else lineHeight = 12
				if text.lineWidthOffset then lineWidthOffset = text.lineWidthOffset else lineWidthOffset = 0

				textLine = {
					text:''
					x:text.x
					y:text.y
					color:text.color
					textAlign:text.textAlign
					textBaseline:text.textBaseline
					fontWeight:text.fontWeight
					fontFamily:text.fontFamily
					fontSize:text.fontSize
				}

				for n in [0..words.length - 1]
					textLine.text = textLine.text  + words[n] + ' '
					lineWidth = ctx.measureText(textLine.text).width
#					console.log textLine.text
					if lineWidth > (text.width / 2 - lineWidthOffset )and n > 0
						$scope.drawText(textLine)
						textLine.y += lineHeight
						textLine.text = ''


			$scope.drawText = (text)->
				ctx = $scope.context
				if !ctx then return
				ctx.save()
				x = text.x
				y = text.y
				if text.color then color = text.color else color = "WHITE"
				#					ctx.font = "normal bold 24px hooge0665"
				ctx.font = $scope.getFont(text)
				if text.textAlign then ctx.textAlign = text.textAlign
				if text.textBaseline then ctx.textBaseline = text.textBaseline
				if text.alpha != undefined then alpha = text.alpha else alpha = 1

				if text.shadow
					ctx.shadowColor = $scope.getColor(text.shadow.color, text.shadow.alpha)
					ctx.shadowOffsetX = text.shadow.offsetX
					ctx.shadowOffsetY = text.shadow.offsetY
					ctx.shadowBlur = text.shadow.blur


				ctx.fillStyle = $scope.getColor(color,alpha)
				ctx.fillText(text.text, x, y)
				ctx.restore()
				return ctx.measureText(text.text).width

			$scope.drawImage = (image, canvasName)->
				if canvasName == SystemParameters.MAINCANVAS
					ctx = $scope.context
				else if canvasName == SystemParameters.ALPHACANVAS
					ctx = $scope.alphaContext
				else if canvasName == SystemParameters.BETACANVAS
					ctx = $scope.betaContext
				else
					ctx = $scope.context

				if !ctx then return

				if image.width then width = image.width else width = image.data.width
				if image.height then height = image.height else height = image.data.height

				ctx.save()
				ctx.drawImage(image.data, image.x, image.y, width, height)
				ctx.restore()

			$scope.drawCSVImage = (image)->
	#					drawSvg will render time by time.
				ctx = $scope.context
				if !ctx then return
				ctx.save()
				ctx.drawSvg(image.data.src, image.x, image.y, image.width, image.height)
				ctx.restore()

			$scope.drawVideoPanel = (videoPanel)->
				if !videoPanel or !videoPanel.video or videoPanel.video.ended then return
				ctx = $scope.context
				ctx.save()
				ctx.drawImage(videoPanel.video,videoPanel.x,videoPanel.y, videoPanel.width, videoPanel.height)
				ctx.restore()

			$scope.drawRetangle = (retangle, canvasName)->

				if canvasName == SystemParameters.MAINCANVAS
					ctx = $scope.context
				else if canvasName == SystemParameters.ALPHACANVAS
					ctx = $scope.alphaContext
				else if canvasName == SystemParameters.BETACANVAS
					ctx = $scope.betaContext
				else
					ctx = $scope.context

				if !ctx then return
				ctx.save()

				x = retangle.x
				y = retangle.y
				width = retangle.width
				height = retangle.height
				if retangle.radius then radius = retangle.radius else radius = 0
				if retangle.strokeColor then strokeColor = retangle.strokeColor else strokeColor = "WHITE"
				if retangle.lineWidth then lineWidth = retangle.lineWidth else lineWidth = 1
				if retangle.fillAlpha then fillAlpha = retangle.fillAlpha else fillAlpha = 1
				if retangle.strokeAlpha then strokeAlpha = retangle.strokeAlpha else strokeAlpha = 1

				ctx.beginPath()
				ctx.moveTo(x + radius, y)
				ctx.lineTo(x + width - radius, y)
				ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
				ctx.lineTo(x + width, y + height - radius)
				ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height)
				ctx.lineTo(x + radius, y + height)
				ctx.quadraticCurveTo(x, y + height, x, y + height - radius)
				ctx.lineTo(x, y + radius)
				ctx.quadraticCurveTo(x, y, x + radius, y)
				ctx.closePath()
				ctx.strokeStyle = $scope.getColor(strokeColor,strokeAlpha)
				ctx.lineWidth = lineWidth
				ctx.stroke()

				if retangle.fill
					if retangle.fillColor then fillColor = retangle.fillColor else fillColor = "WHITE"

					ctx.fillStyle = $scope.getColor(fillColor,fillAlpha)
					ctx.fillRect(x,y,width,height)
				ctx.restore()

			$scope.drawPolygon = (polygon)->
	#					console.log polygon
				if polygon.strokeColor then strokeColor = polygon.strokeColor else strokeColor = "WHITE"
				if polygon.lineWidth then lineWidth = polygon.lineWidth else lineWidth = 1
				points = polygon.points
				ctx = $scope.context
				ctx.save()
				ctx.beginPath()
				for i in [0..points.length - 1] by 1
					if i == 0
						ctx.moveTo(points[i].x, points[i].y)
					else
						ctx.lineTo(points[i].x, points[i].y)
				ctx.closePath()
				ctx.strokeStyle = $scope.getColor(strokeColor)
				ctx.lineWidth = lineWidth
				ctx.stroke()

				if polygon.fill
					if polygon.fillColor then fillColor = polygon.fillColor else fillColor = "WHITE"
					ctx.fillStyle = $scope.getColor(fillColor)
					ctx.fill()
				ctx.restore()

			$scope.drawCircle = (circle, canvasName)->

				if canvasName == SystemParameters.MAINCANVAS
						ctx = $scope.context
				else if canvasName == SystemParameters.ALPHACANVAS
					ctx = $scope.alphaContext
				else if canvasName == SystemParameters.BETACANVAS
					ctx = $scope.betaContext
				else
					ctx = $scope.context

				if !ctx then return

				ctx.save()
				x = circle.x
				y = circle.y
				r = circle.r
				if circle.lineWidth then lineWidth = circle.lineWidth else lineWidth = 1
				if circle.strokeColor then strokeColor = circle.strokeColor else strokeColor = "WHITE"

				ctx.beginPath()
				ctx.arc(x, y, r, 2*Math.PI, false)
				ctx.closePath()
				ctx.strokeStyle = strokeColor
				ctx.lineWidth = lineWidth
				ctx.stroke()

				if circle.fill
					if circle.fillColor then fillColor = circle.fillColor else fillColor = "WHITE"
					ctx.fillStyle = $scope.getColor(fillColor)
					ctx.fill()

				ctx.restore()


			$scope.drawLine = (line)->
				ctx = $scope.context
				ctx.save()
				if line.lineWidth then lineWidth = line.lineWidth else lineWidth = 1
				if line.strokeColor then strokeColor = line.strokeColor else strokeColor = "WHITE"

				ctx.beginPath()
				ctx.moveTo(line.startX,line.startY)
				ctx.lineTo(line.endX,line.endY)
				ctx.closePath()
				ctx.lineWidth = lineWidth
				ctx.strokeStyle = $scope.getColor(strokeColor)
				ctx.stroke()
				ctx.restore()

			$scope.drawPanelButton = (button)->
				if button.visible == false then return
				ctx = $scope.context
				ctx.save()
				$scope.drawImage(button)
				ctx.restore()


			$scope.createRetangleMask = (retangle)->
				if !retangle then return

				ctx = $scope.context
				ctx.save()
				x = retangle.x
				y = retangle.y
				width = retangle.width
				height = retangle.height
				ctx.beginPath()
				ctx.rect(x,y,width,height)
				ctx.closePath()
				ctx.clip()

			$scope.endMask = ()->
				ctx = $scope.context
				if !ctx then return
				ctx.restore()

	#------------------- event handles  ---------------------------


			$scope.loadImageData = (image, callback)->
				image.data = new Image()
				image.data.onload = ()->
					if this.complete
						if callback
							callback()
				image.data.src = image.url

			$scope.checkMouseAndHitspotCollision = (e,hitspot)->
				if !hitspot then return

				mouse = $scope.getMousePosition(e)
				mouseX = mouse.x
				mouseY = mouse.y
				if mouseX >= hitspot.x and mouseX <= hitspot.x + hitspot.width
					if mouseY >= hitspot.y and mouseY <= hitspot.y + hitspot.height
						return true

				return false

			$scope.getMousePosition = (e) ->
				if !$scope.canvas then return

				bbox = $scope.canvas.getBoundingClientRect()
	#					console.log bbox
				mouse = {
					x:e.clientX - bbox.left * ($scope.canvas.width / bbox.width),
					y:e.clientY - bbox.top * ($scope.canvas.height / bbox.height)
				}
	#					console.log mouse
				return mouse

#------------------------------ private functions ----------------------------------
			addShowingPanel = (newPanel)->
				newPanel.editDateTime = Date.now()
				ResourceModel.showingPanels.push(newPanel)

#------------------------------ event handle functions ------------------------------
			$scope.$on(EVENT.PLAY_DYNASTY_INTRO_AUDIO, (e, introAudio)->
				e.preventDefault()

				ResourceModel.currentIntroAudio = introAudio

				onPlayingCallback = ()->
#					console.log "playing"
				onFinishCallback = ()->
					if !ResourceModel.currentDynasty.introAudios then return
					introAudio.state = SystemParameters.introAudioState.COMPLETED
					nextIntroIndex = ResourceModel.currentIntroAudio.id + 1
					if nextIntroIndex < ResourceModel.currentDynasty.introAudios.length
						ResourceModel.currentIntroAudio = ResourceModel.currentDynasty.introAudios[nextIntroIndex]
						SoundManager.playIntroAudio(ResourceModel.currentIntroAudio, onPlayingCallback,onFinishCallback)
						DynastyIntroAudioControl.fadeInText()
					else
						ResourceModel.currentIntroAudio = undefined
						ResourceModel.drawingIntroText = undefined

#				introAudio.state = SystemParameters.introAudioState.PLAYING
				SoundManager.playIntroAudio(introAudio,onPlayingCallback,onFinishCallback )
				DynastyIntroAudioControl.fadeInText()
			)

			$scope.$on(EVENT.COMPLETE_DYNASTY_INTRO_AUDIO, (e, introAudio)->
				e.preventDefault()
			)


			$scope.$on(EVENT.ON_CHANGE_CURRENT_DYNASTY, (e, hitspot)->
				e.preventDefault()

				changingDynasty = (ResourceModel.currentDynastyId != hitspot.dynastyId or !ResourceModel.currentDynastyId)

				ResourceModel.currentDynastyId = hitspot.dynastyId
				ResourceModel.currentDynastyPeriod = hitspot.periodId

				for dynasty in ResourceModel.dynasties
					if dynasty.id == ResourceModel.currentDynastyId
						ResourceModel.currentDynasty = dynasty
						if changingDynasty
							if ResourceModel.currentIntroAudio
								SoundManager.stopIntroAudio(ResourceModel.currentIntroAudio)
								ResourceModel.currentIntroAudio = undefined
								ResourceModel.drawingIntroText = undefined
							DynastyIntroAudioControl.load($scope)
						break

				DynastyMapControl.changeCurrentMap($scope)
				NavigationPanelControl.changeCurrentDynasty()
				$scope.$emit(EVENT.CLEAR_SHOWING_PANELS)

			)

			$scope.$on(EVENT.CLEAR_SHOWING_PANELS, (e)->
				e.preventDefault()
				showingPanels = ResourceModel.showingPanels
				for showingPanel in showingPanels
					showingPanel.resource.isShowing = false

				ResourceModel.showingPanels = null
				ResourceModel.showingPanels = []

				showingPanelContainers = SystemParameters.panelSettings.showingPanelContainers
				for container in showingPanelContainers
					container.panel = undefined

				$scope.$apply(ResourceModel.showingPanels)

			)

			$scope.$on(EVENT.REMOVE_SHOWING_PANEL, (e, data)->
				e.preventDefault()
				if !data.panel then return

				if data.panelContainer
					data.panelContainer.panel = undefined
				else
					for container in SystemParameters.panelSettings.showingPanelContainers
						if container.panel == data.panel
							container.panel = undefined
							break

				panel = data.panel
				panel.resource.isShowing = false
				ResourceModel.showingPanels.splice(ResourceModel.showingPanels.indexOf(panel),1)
				panel = undefined

				$scope.$apply(ResourceModel.showingPanels)
				if data.callback then data.callback()
			)

			$scope.$on(EVENT.ADD_SHOWING_PANEL, (e, newPanel)->
				e.preventDefault()
				newPanel.resource.isShowing = true
				addShowingPanel(newPanel)
				$scope.$apply(ResourceModel.showingPanels)
			)

			$scope.$on(EVENT.MAXIMIZE_TEXT_PANEL, (e, panel)->
				e.preventDefault()
				$scope.$emit(EVENT.PAUSE_ALL_SHOWING_VIDEO_PANEL)
				MaximizedPanelControl.creatingMaximizedTextPanel($scope, panel, ()->
#					panel.visible = false
				)
			)

			$scope.$on(EVENT.MAXIMIZE_IMAGE_PANEL, (e, panel)->
				e.preventDefault()
				$scope.$emit(EVENT.PAUSE_ALL_SHOWING_VIDEO_PANEL)
				MaximizedPanelControl.creatingMaximizedImagePanel($scope, panel, ()->
#					panel.visible = false
				)
			)

			$scope.$on(EVENT.MAXIMIZE_VIDEO_PANEL, (e, panel)->
				e.preventDefault()
				$scope.$emit(EVENT.PAUSE_ALL_SHOWING_VIDEO_PANEL)
				MaximizedPanelControl.creatingMaximizedVideoPanel($scope, panel, ()->
#					panel.visible = false
					panel.video.play()
				)
			)

			$scope.$on(EVENT.REMOVE_SHOWING_MAXIMAIZE_PANEL, (e, data)->
				e.preventDefault()
				ResourceModel.showingMaximizedPanel = undefined
				$scope.$emit(EVENT.PLAY_ALL_SHOWING_VIDEO_PANEL)
			)

			$scope.$on(EVENT.PAUSE_ALL_SHOWING_VIDEO_PANEL, (e)->
				e.preventDefault()
				ShowingPanelsControl.pauseAllPlayingVideo()
			)

			$scope.$on(EVENT.PLAY_ALL_SHOWING_VIDEO_PANEL, (e)->
				e.preventDefault()
				ShowingPanelsControl.playAllPausedVideo()
			)

			$scope.$on(EVENT.ON_AINIMATION_PLAYING, (e)->
				e.preventDefault()
				SystemParameters.onPlayingAnimation = true
			)

			$scope.$on(EVENT.ON_AINIMATION_COMPLETED, (e)->
				e.preventDefault()
				SystemParameters.onPlayingAnimation = false
			)

			$scope.$on(EVENT.UPDATE_POINTER_POSITION, (e, data)->
				e.preventDefault()

				NavigationPanelControl.updateLocation(data)

			)

	]