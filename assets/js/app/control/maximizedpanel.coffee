define ['angular', 'appmodule', 'angular_resource'], =>
	appModule = angular.module 'app'
	appModule.factory 'MaximizedPanelControl', ['EVENT','SystemParameters','ResourceModel',(EVENT,SystemParameters,ResourceModel) =>
		{
			draw:($scope)->
				if !ResourceModel.showingMaximizedPanel then return

				ctx = $scope.context
				if !ctx	then	return

				drawMaximizedTextPanel = (panel)->
					if !panel.visible then return

					$scope.drawRetangle(panel.frame)
					$scope.createRetangleMask(panel.mask)
					$scope.drawParagraph(panel.text)
					$scope.endMask()
					$scope.drawPanelButton(panel.buttonClose)
					if panel.buttonMaximize
						$scope.drawPanelButton(panel.buttonMaximize)
					if panel.buttonRestoreDown
						$scope.drawPanelButton(panel.buttonRestoreDown)

				drawMaximizedImagePanel = (panel)->
					if !panel.visible then return

					$scope.drawRetangle(panel.frame)
					$scope.createRetangleMask(panel.mask)
					$scope.drawImage(panel.image)
					$scope.endMask()
					$scope.drawPanelButton(panel.buttonClose)
					if panel.buttonMaximize
						$scope.drawPanelButton(panel.buttonMaximize)
					if panel.buttonRestoreDown
						$scope.drawPanelButton(panel.buttonRestoreDown)

				drawMaximizedVideoPanel = (panel)->
					if !panel.visible then return

					$scope.drawRetangle(panel.frame)
					$scope.createRetangleMask(panel.mask)
					$scope.drawVideoPanel(panel)
					$scope.endMask()
					$scope.drawPanelButton(panel.buttonClose)
					if panel.buttonMaximize
						$scope.drawPanelButton(panel.buttonMaximize)
					if panel.buttonRestoreDown
						$scope.drawPanelButton(panel.buttonRestoreDown)
					if panel.buttonPause
						$scope.drawPanelButton(panel.buttonPause)
					if panel.buttonPlay
						$scope.drawPanelButton(panel.buttonPlay)

				drawMaximizedPanel = ()->
					if !ResourceModel.showingMaximizedPanel then return
					resourceTypeId = ResourceModel.showingMaximizedPanel.originalPanel.resource.resourceTypeId
					panel = ResourceModel.showingMaximizedPanel

					switch resourceTypeId
						when SystemParameters.RESOURCE_TYPE.TEXT then drawMaximizedTextPanel(panel)
						when SystemParameters.RESOURCE_TYPE.IMAGE then drawMaximizedImagePanel(panel)
						when SystemParameters.RESOURCE_TYPE.VIDEO then drawMaximizedVideoPanel(panel)
						else drawResourceTextPanel()

				drawMaximizedPanel()

# --------------------------------------  end draw ---------------------------------------------

			creatingMaximizedTextPanel:($scope, originalPanel,callback)->

				panelSettings = SystemParameters.panelSettings
				panelModel = SystemParameters.panelSettings.maximizedPanel

				panelX = panelModel.x
				panelY = panelModel.y
				padding = panelModel.padding
				panelWidth = panelModel.width
				panelHeight = panelModel.height

				textPanel = {
					visible:true
					originalPanel: originalPanel
					text:{
						text:originalPanel.resource.description
						x:panelX + padding
						y:panelY + padding
						width:panelWidth - padding * 2
						height:panelHeight - padding * 2
						color: originalPanel.text.color
						fontWeight:originalPanel.text.fontWeight
						fontFamily:originalPanel.text.fontFamily
						fontSize:panelModel.fontSize
						lineHeight:originalPanel.text.lineHeight
						textBaseline:originalPanel.text.textBaseline
						lineWidthOffset:originalPanel.lineWidthOffset
					}

					frame:{
						x:panelX
						y:panelY
						width:panelWidth
						height:panelHeight
						lineWidth:originalPanel.frame.lineWidth
						strokeColor:originalPanel.frame.strokeColor
						strokeAlpha:originalPanel.frame.strokeAlpha
						fillColor:originalPanel.frame.fillColor
						fillAlpha:originalPanel.frame.fillAlpha
						fill:originalPanel.frame.fill
					}

					mask:{
						x:panelX + padding
						y:panelY + padding
						width:panelWidth - padding * 2
						height:panelHeight - padding * 2
					}

					buttonClose:{
						data:$scope.getImageByName(panelSettings.buttonClose.buttonName).data
						x:panelX + panelWidth - panelSettings.maximizedButton.width
						y:panelY
						width:panelSettings.maximizedButton.width
						height:panelSettings.maximizedButton.height
						hitspot:{
							x: panelX + panelWidth - panelSettings.maximizedButton.width
							y: panelY
							width: panelSettings.maximizedButton.width
							height: panelSettings.maximizedButton.height
						}
						visible:true
					}

					buttonRestoreDown:{
						data: $scope.getImageByName(panelSettings.buttonRestoreDown.buttonName).data
						x:panelX + panelWidth - panelSettings.maximizedButton.width
						y:panelY + panelSettings.maximizedButton.height
						width:panelSettings.maximizedButton.width
						height:panelSettings.maximizedButton.height
						hitspot:{
							x: panelX + panelWidth - panelSettings.maximizedButton.width
							y: panelY + panelSettings.maximizedButton.height
							width: panelSettings.maximizedButton.width
							height: panelSettings.maximizedButton.height
						}
						visible:true
					}

					hitspot:{
						x:panelX
						y:panelY
						width: panelWidth
						height: panelHeight
					}

				}

				ResourceModel.showingMaximizedPanel = textPanel

				if callback then callback()

			creatingMaximizedImagePanel:($scope, originalPanel,callback)->

				panelSettings = SystemParameters.panelSettings
				panelModel = SystemParameters.panelSettings.maximizedPanel

				panelX = panelModel.x
				panelY = panelModel.y
				padding = panelModel.padding
				panelWidth = panelModel.width
				panelHeight = panelModel.height

				newImagePanel = {
					visible:true
					originalPanel: originalPanel
					image:{
						x:panelX + padding
						y:panelY + padding
						width:panelWidth - padding * 2
						height:panelHeight - padding * 2
						data:originalPanel.image.data
					}

					frame:{
						x:panelX
						y:panelY
						width:panelWidth
						height:panelHeight
						lineWidth:originalPanel.frame.lineWidth
						strokeColor:originalPanel.frame.strokeColor
						strokeAlpha:originalPanel.frame.strokeAlpha
						fillColor:originalPanel.frame.fillColor
						fillAlpha:originalPanel.frame.fillAlpha
						fill:originalPanel.frame.fill
					}

					mask:{
						x:panelX + padding
						y:panelY + padding
						width:panelWidth - padding * 2
						height:panelHeight - padding * 2
					}

					buttonClose:{
						data:$scope.getImageByName(panelSettings.buttonClose.buttonName).data
						x:panelX + panelWidth - panelSettings.maximizedButton.width
						y:panelY
						width:panelSettings.maximizedButton.width
						height:panelSettings.maximizedButton.height
						hitspot:{
							x: panelX + panelWidth - panelSettings.maximizedButton.width
							y: panelY
							width: panelSettings.maximizedButton.width
							height: panelSettings.maximizedButton.height
						}
						visible:true
					}

					buttonRestoreDown:{
						data: $scope.getImageByName(panelSettings.buttonRestoreDown.buttonName).data
						x:panelX + panelWidth - panelSettings.maximizedButton.width
						y:panelY + panelSettings.maximizedButton.height
						width:panelSettings.maximizedButton.width
						height:panelSettings.maximizedButton.height
						hitspot:{
							x: panelX + panelWidth - panelSettings.maximizedButton.width
							y: panelY + panelSettings.maximizedButton.height
							width: panelSettings.maximizedButton.width
							height: panelSettings.maximizedButton.height
						}
						visible:true
					}

					hitspot:{
						x:panelX
						y:panelY
						width: panelWidth
						height: panelHeight
					}

				}

				ResourceModel.showingMaximizedPanel = newImagePanel

				if callback then callback()

			creatingMaximizedVideoPanel:($scope, originalPanel,callback)->

				panelSettings = SystemParameters.panelSettings
				panelModel = SystemParameters.panelSettings.maximizedPanel

				panelX = panelModel.x
				panelY = panelModel.y
				padding = panelModel.padding
				panelWidth = panelModel.width
				panelHeight = panelModel.height

				newVideoPanel = {
					visible:true
					originalPanel: originalPanel
					videoId:originalPanel.videoId
					video: originalPanel.video
					x:panelX + padding
					y:panelY + padding
					width:panelWidth - padding * 2
					height:panelHeight - padding * 2

					frame:{
						x:panelX
						y:panelY
						width:panelWidth
						height:panelHeight
						lineWidth:originalPanel.frame.lineWidth
						strokeColor:originalPanel.frame.strokeColor
						strokeAlpha:originalPanel.frame.strokeAlpha
						fillColor:originalPanel.frame.fillColor
						fillAlpha:originalPanel.frame.fillAlpha
						fill:originalPanel.frame.fill
					}

					mask:{
						x:panelX + padding
						y:panelY + padding
						width:panelWidth - padding
						height:panelHeight - padding
					}

					buttonClose:{
						data:$scope.getImageByName(panelSettings.buttonClose.buttonName).data
						x:panelX + panelWidth - panelSettings.maximizedButton.width
						y:panelY
						width:panelSettings.maximizedButton.width
						height:panelSettings.maximizedButton.height
						hitspot:{
							x: panelX + panelWidth - panelSettings.maximizedButton.width
							y: panelY
							width: panelSettings.maximizedButton.width
							height: panelSettings.maximizedButton.height
						}
						visible:true
					}

					buttonRestoreDown:{
						data: $scope.getImageByName(panelSettings.buttonRestoreDown.buttonName).data
						x:panelX + panelWidth - panelSettings.maximizedButton.width
						y:panelY + panelSettings.maximizedButton.height
						width:panelSettings.maximizedButton.width
						height:panelSettings.maximizedButton.height
						hitspot:{
							x: panelX + panelWidth - panelSettings.maximizedButton.width
							y: panelY + panelSettings.maximizedButton.height
							width: panelSettings.maximizedButton.width
							height: panelSettings.maximizedButton.height
						}
						visible:true
					}

					buttonPause:{
						data:$scope.getImageByName(panelSettings.buttonPause.buttonName).data
						x:panelX + panelWidth - panelSettings.maximizedButton.width
						y:panelY + panelSettings.maximizedButton.height * 2 - 5
						width:panelSettings.maximizedButton.width
						height:panelSettings.maximizedButton.height
						hitspot:{
							x: panelX + panelWidth - panelSettings.maximizedButton.width
							y:panelY + + panelSettings.maximizedButton.height * 2
							width: panelSettings.maximizedButton.width
							height: panelSettings.maximizedButton.height
						}
						visible:true
					}

					buttonPlay:{
						data: $scope.getImageByName(panelSettings.buttonPlay.buttonName).data
						x:panelX + panelWidth - panelSettings.maximizedButton.width
						y:panelY + panelSettings.maximizedButton.height * 2 - 5
						width:panelSettings.maximizedButton.width
						height:panelSettings.maximizedButton.height
						hitspot:{
							x: panelX + panelWidth - panelSettings.maximizedButton.width
							y:panelY + + panelSettings.maximizedButton.height * 2
							width: panelSettings.maximizedButton.width
							height: panelSettings.maximizedButton.height
						}
						visible:false
					}

					hitspot:{
						x:panelX
						y:panelY
						width: panelWidth
						height: panelHeight
					}

				}

				ResourceModel.showingMaximizedPanel = newVideoPanel

				if callback then callback()

			handleMouseUp:($scope,e)->

				if !ResourceModel.showingMaximizedPanel  then return
				maximizedPanel = ResourceModel.showingMaximizedPanel

				if $scope.checkMouseAndHitspotCollision(e,maximizedPanel.buttonClose.hitspot)
					data = {
						originalPanel:maximizedPanel.originalPanel
					}
					$scope.$emit(EVENT.REMOVE_SHOWING_MAXIMAIZE_PANEL, data)

					data = {
						panel:maximizedPanel.originalPanel
					}
					$scope.$emit(EVENT.REMOVE_SHOWING_PANEL, data)
					return

				if $scope.checkMouseAndHitspotCollision(e,maximizedPanel.buttonRestoreDown.hitspot)
					data = {
						originalPanel:maximizedPanel.originalPanel
					}
					$scope.$emit(EVENT.REMOVE_SHOWING_MAXIMAIZE_PANEL, data)
					return

				if maximizedPanel.buttonPlay?.visible
					if $scope.checkMouseAndHitspotCollision(e,maximizedPanel.buttonPlay.hitspot)
						maximizedPanel.video.play()
						maximizedPanel.buttonPlay.visible = false
						maximizedPanel.buttonPause.visible = true
				else if maximizedPanel.buttonPause?.visible
					if $scope.checkMouseAndHitspotCollision(e,maximizedPanel.buttonPause.hitspot)
						maximizedPanel.video.pause()
						maximizedPanel.buttonPlay.visible = true
						maximizedPanel.buttonPause.visible = false

		}
	]