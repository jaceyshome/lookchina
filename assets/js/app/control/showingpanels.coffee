define ['angular', 'appmodule', 'angular_resource'], =>
	appModule = angular.module 'app'
	appModule.factory 'ShowingPanelsControl', ['EVENT','SystemParameters','ResourceModel', 'ShowingTextPanelControl','ShowingVideoPanelControl','ShowingImagePanelControl',(EVENT,SystemParameters,ResourceModel,ShowingTextPanelControl,ShowingVideoPanelControl,ShowingImagePanelControl) =>
		{
		draw: ($scope)->
			if ResourceModel.showingPanels.length == 0 then return
			ctx = $scope.context
			if !ctx	then	return

			drawResourceTextPanel = (panel)->
				if !panel.visible then return

				$scope.drawRetangle(panel.frame)
				#				$scope.drawRetangle(panel.hitspot)
				$scope.createRetangleMask(panel.mask)
				$scope.drawParagraph(panel.text)
				$scope.endMask()
				$scope.drawPanelButton(panel.buttonClose)
				$scope.drawPanelButton(panel.buttonMaximize)
			#				$scope.drawRetangle(panel.buttonClose.hitspot)

			drawResourceImagePanel = (panel)->
				if !panel.visible then return

				$scope.drawRetangle(panel.frame)
				#				$scope.drawRetangle(panel.hitspot)
				$scope.createRetangleMask(panel.mask)
				$scope.drawImage(panel.image)
				$scope.endMask()
				$scope.drawPanelButton(panel.buttonClose)
				$scope.drawPanelButton(panel.buttonMaximize)

			drawResourceVideoPanel = (panel)->
				if !panel.visible then return

				$scope.drawRetangle(panel.frame)
				#				$scope.drawRetangle(panel.hitspot)
				$scope.createRetangleMask(panel.mask)
				$scope.drawVideoPanel(panel)
				$scope.endMask()
				$scope.drawPanelButton(panel.buttonClose)
				$scope.drawPanelButton(panel.buttonMaximize)
				$scope.drawPanelButton(panel.buttonPause)
				$scope.drawPanelButton(panel.buttonPlay)

			drawShowingResourcePanels = ()->

				for panel in ResourceModel.showingPanels
					switch panel.resource.resourceTypeId
						when SystemParameters.RESOURCE_TYPE.TEXT then drawResourceTextPanel(panel)
						when SystemParameters.RESOURCE_TYPE.IMAGE then drawResourceImagePanel(panel)
						when SystemParameters.RESOURCE_TYPE.VIDEO then drawResourceVideoPanel(panel)
						else drawResourceTextPanel()

			drawShowingResourcePanels()

#---------------------------------------- end of draw ---------------------------------------

		createShowingPanel:($scope,resource)->

			removeOldestShowingPanel = (callback)->
				showingPanelContainers = SystemParameters.panelSettings.showingPanelContainers

				for i in [0..showingPanelContainers.length - 1] by 1
					if showingPanelContainers[i].panel == ResourceModel.showingPanels[0]
						data = {}
						data.panel = showingPanelContainers[i].panel
						#						data.panelContainer = showingPanelContainers[i]
						data.callback = callback
						#						showingPanelContainers[i].panel = undefined
						$scope.$emit(EVENT.REMOVE_SHOWING_PANEL, data)
						break

			prepareNewShowingPanel = (callback)->
				showingPanels = ResourceModel.showingPanels
				if showingPanels.length < SystemParameters.panelSettings.totalShowingPanel
					callback()
					return
				else
					removeOldestShowingPanel(callback)


			getNewShowingPanelContainer = ()->
				containers = SystemParameters.panelSettings.showingPanelContainers
				container = {}

				for container in containers
					if container.panel == undefined
						return container

			getPositionFromContainer = (container)->
#TODO make only one panel and show it in the center
#				switch ResourceModel.showingPanels.length
#					when 0
#						_x = ( containers[1].x - containers[0].x ) / 2 + containers[0].x
#						_y = ( containers[2].y - containers[0].y ) / 2 + containers[0].y
#						return container = {x:_x, y:_y}
#
#					when 1
#						_x = containers[1].x
#						_y = ( containers[2].y - containers[0].y ) / 2 + containers[0].y
#						return container = {x:_x, y:_y}
#
#					when 2
#						_x = containers[2].x
#						_y = containers[2].y
#						return container = {x:_x, y:_y}
#
#					when 3
#						for container in containers
#							if container.panel == undefined
#								return container
#
#					when 4
				position = {
					x:container.x
					y:container.y
				}

				return position

			#------------------------------ private functions ----------------------------------




			for resourceType in ResourceModel.resourceTypes
				if resourceType.id == resource.resourceTypeId
					typeName = resourceType.name
					prepareNewShowingPanel(()->
						container = getNewShowingPanelContainer()
						position = getPositionFromContainer(container)
						switch typeName
							when 'text'
								ShowingTextPanelControl.createPanel($scope,resource,position,container)
								break
							when 'video'
								ShowingVideoPanelControl.createPanel($scope,resource,position, container)
								break
							when 'image'
								ShowingImagePanelControl.createPanel($scope,resource,position, container)
								break
							else
								ShowingTextPanelControl.createPanel($scope,resource,position,container)
								break
					)
					break



		handleMouseMove:()->
			undefined

		handleMouseUp:($scope,e,showingPanel)->
			if !showingPanel.resource then return
			resource = showingPanel.resource
			console.log "showing resource:", resource
			for resourceType in ResourceModel.resourceTypes
				if resourceType.id == resource.resourceTypeId
					typeName = resourceType.name
					switch typeName
						when 'text'
							ShowingTextPanelControl.handleMouseUp($scope,e,showingPanel)
							break
						when 'video'
							ShowingVideoPanelControl.handleMouseUp($scope,e,showingPanel)
							break
						when 'image'
							ShowingImagePanelControl.handleMouseUp($scope,e,showingPanel)
							break
						else
							ShowingTextPanelControl.handleMouseUp($scope,e,showingPanel)
							break
			undefined


		pauseAllPlayingVideo:()->
			if ResourceModel.showingPanels.length == 0 then return


			for showingPanel in ResourceModel.showingPanels
				if showingPanel.video
					showingPanel.video.pause()

		playAllPausedVideo:()->
			if ResourceModel.showingPanels.length == 0 then return

			for showingPanel in ResourceModel.showingPanels
				if showingPanel.video
					showingPanel.video.play()










		}
	]