define ['angular', 'appmodule', 'angular_resource'], =>
	appModule = angular.module 'app'
	appModule.factory 'NavigationPanelControl', ['SystemParameters', 'ResourceModel',(SystemParameters, ResourceModel) =>
		return {

			update:($scope)->
				navigationPanel = SystemParameters.navigationPanel
				if !navigationPanel then return

				updateSoundEffect = ()->
					soundEffect = navigationPanel.soundEffect
					if !soundEffect then return

					updateLineChart = ()->
						lineChart = soundEffect.lineChart
						eqData = $scope.eqData

						if !$scope.eqData
							for cylinder in lineChart.cylinders
								cylinder.percentage = 0
						else
							left = eqData.left
							right = eqData.right

							cylinderIndex = 0
							cylinders = lineChart.cylinders

							if !left || left.length == 0
								for cylinderIndex in [0..(cylinders.length / 2 - 1)] by 1
									cylinders[cylinderIndex].percentage = 0
							else
								for i in [0..left.length - 1] by 60
									cylinders[cylinderIndex].percentage = left[i]
									cylinderIndex++

							if !right || right.length == 0
								for cylinderIndex in [cylinders.length / 2 - 1 .. cylinders.length - 1] by 1
									cylinders[cylinderIndex].percentage = 0
							else
								for j in [0..right.length - 1] by 60
									cylinders[cylinderIndex].percentage = right[j]
									cylinderIndex++


					updateLineChart()

				updateSoundEffect()






#	----------------------- end of update -------------------------------------

			draw: ($scope)->

				canvas = $scope.canvas
				ctx = $scope.context

				if!ctx then return

				drawdashboard = ()->

# ---------------------- property -----------------------------

					navigationPanel = SystemParameters.navigationPanel
					if !navigationPanel then return

# ---------------------- functions ---------------------------

					drawPowerChart = ()->
						chart = navigationPanel.power.percentage
						if !chart then return
						model = chart.chartModel
						if !model then return

						total = chart.value / Math.abs(model.step)

						for index in [1..total]
							retangle = {
								x : model.startX + (model.gap + model.width )* index * model.xOffset
								y : model.startY + (model.gap + model.height )* index * model.yOffset
								width: model.width
								height:model.height
								fill: model.fill
							}
							$scope.drawRetangle(retangle)

					drawLocation = ->
						location = navigationPanel.location
						if !location then return

						$scope.drawText(location.n.label)
						$scope.drawText(location.n.text)
						$scope.drawText(location.e.label)
						$scope.drawText(location.e.text)
						$scope.drawCircle(location.circle)
						$scope.drawCircle(location.pointer)

					drawProgress = ->
						progress = navigationPanel.progress
						if !progress then return

						$scope.drawText(progress.label)
						$scope.drawText(progress.percentage)

						state = progress.state

						wholeLine = {
							startX: state.x
							startY: state.y
							endX: state.x + state.width
							endY: state.y
							strokeColor:"WHITE"
							lineWidth:state.lineWidth
						}

						completedLine = {
							startX: state.x
							startY: state.y
							endX: state.x + state.width * state.complete
							endY: state.y
							strokeColor:state.completeStrokeColor
							lineWidth:state.completeLineWidth
						}

						$scope.drawLine(wholeLine)
						$scope.drawLine(completedLine)

						pointer = {
							x:completedLine.endX - state.pointer.width / 2
							y:completedLine.endY - state.pointer.height / 2
							width: state.pointer.width
							height: state.pointer.height
							fillColor: state.pointer.fillColor
							strokeColor: state.pointer.strokeColor
							fill:state.pointer.fill
						}

						$scope.drawRetangle(pointer)

					drawTopicStates = ->
						states = navigationPanel.topicStates
						if !states then return

						for state in states
							$scope.drawText(state.label)

							width = 8
							height = 6
							gap = 5
							offset = 0

							statePercentage = {
								x:state.label.x - width - gap
								y:state.label.y - (height * state.statePercentage.completePercentage) - offset
								width:width
								height:height * state.statePercentage.completePercentage
								fillColor: "SKYBLUE"
								strokeColor: "SKYBLUE"
								lineWidth: 0
								fill:true
							}
							$scope.drawRetangle(statePercentage)

					drawFolderLabels = ->
						folderLabels = navigationPanel.folderLabels
						if !folderLabels then return

						for folderLabel in folderLabels
							$scope.drawText(folderLabel)

					drawLineCharts = (lineChart)->
						if !lineChart then return

						cylinders = lineChart.cylinders
						startX = lineChart.startX
						startY = lineChart.startY
						gap = lineChart.gap
						width = lineChart.width
						height = lineChart.height

						for cylinder in cylinders
							cylinder.fill = true
							cylinder.lineWidth = 1
							cylinder.points = [
								{x:startX + gap, y:startY + height * (1 - cylinder.percentage )}
								{x:startX + gap, y:startY + height}
								{x:startX + gap + width, y:startY + height}
								{x:startX + gap + width, y:startY + height * (1 - cylinder.percentage )}
							]
							$scope.drawPolygon(cylinder)
							startX = startX + width + gap

					drawSoundEffect = ->
						soundEffect = navigationPanel.soundEffect
						if !soundEffect then return

						$scope.drawRetangle(soundEffect.retangleMarker)
						$scope.drawPolygon(soundEffect.polygonMarker)
#						$scope.drawLine(soundEffect.underline)
						drawLineCharts(soundEffect.lineChart)

					drawDateAndTime = ->
						dateAndTime = navigationPanel.dateAndTime
						if !dateAndTime then return

						$scope.drawRetangle(dateAndTime.retangle)
						$scope.drawText(dateAndTime.date)
						$scope.drawText(dateAndTime.time)

					drawSignal = ->
						signal = navigationPanel.signal
						if !signal then return

						for line in signal.icon.lines
							$scope.drawLine(line)

						drawLineCharts(signal.bar)

					drawConnect = ->
						connect = navigationPanel.connect
						if !connect then return

						if(connect.bgImage == undefined )
							connect.bgImage = $scope.getImageByName('connect')
						$scope.drawImage(connect.bgImage)

						if(connect.circles.length == 0)
							startX = connect.startX
							y = connect.y
							gap = connect.gap
							for i in [0..connect.totalNumber - 1]
								connect.circles.push(
									{
										x: startX + gap
										y: y
										fill:false
										fillColor:connect.fillColor
										strokeColor:connect.strokeColor
										index:i
										r: connect.r
									})
								startX = startX + gap

						for circle in connect.circles
							if(circle.index == 2)
								circle.fill = true
							$scope.drawCircle(circle)

					# -------------- main body ------------------------------------

					if(navigationPanel.bgImage == undefined )
						navigationPanel.bgImage = $scope.getImageByName('dashboard_bg')
					$scope.drawImage(navigationPanel.bgImage)

					if(navigationPanel.spaceship == undefined )
						navigationPanel.spaceship = $scope.getImageByName('dashboard_spaceship')
					$scope.drawImage(navigationPanel.spaceship)

					$scope.drawText(navigationPanel.userName)

					if navigationPanel.title.text
						navigationPanel.title.text = navigationPanel.title.text.toUpperCase()
						$scope.drawText(navigationPanel.title)
						$scope.drawText(navigationPanel.subTitle)

						$scope.drawText(navigationPanel.startYear)
						$scope.drawText(navigationPanel.startYearPeriod)
						$scope.drawText(navigationPanel.endYear)
						$scope.drawText(navigationPanel.endYearPeriod)

					$scope.drawRetangle(navigationPanel.power.frame)
					$scope.drawText(navigationPanel.power.label)
					$scope.drawText(navigationPanel.power.percentage)
					drawPowerChart()
					drawLocation()
					drawProgress()
					drawTopicStates()
					drawFolderLabels()
					$scope.drawText(navigationPanel.level)
					$scope.drawText(navigationPanel.logoutButton)

					drawSoundEffect()
					drawDateAndTime()
					drawSignal()
					drawConnect()

				drawdashboard()

#------------------------------------- hanle changes -------------------------------
			changeCurrentDynasty:()->
				navigationPanel = SystemParameters.navigationPanel
				if !navigationPanel then return
				if !ResourceModel.currentDynasty then return

				currentDynasty = ResourceModel.currentDynasty

				updateTitle = ()->
					navigationPanel.title.text = currentDynasty.name

				updatePeriod = ()->
					totalPeriod = currentDynasty.totalPeriod
					averageYears = Math.floor((Math.abs(currentDynasty.endYear - currentDynasty.startYear) ) / totalPeriod )
					currentPeriod = ResourceModel.currentDynastyPeriod
					currentStartYear = currentDynasty.startYear + averageYears * currentPeriod
					if currentPeriod + 1 != totalPeriod
						currentEndYear = currentDynasty.startYear + averageYears * (currentPeriod + 1)
					else
						currentEndYear = currentDynasty.endYear

					navigationPanel.startYear.text = Math.abs(currentStartYear)
					navigationPanel.startYearPeriod.text = if currentStartYear >= 0 then "DC -" else "BC -"
					navigationPanel.endYear.text = Math.abs(currentEndYear)
					navigationPanel.endYearPeriod.text = if currentEndYear >= 0 then "DC" else "BC"


# ----------------------  execution body ------------------------

				updateTitle()
				updatePeriod()

# ----------------------------  event handles -----------------------

		handleMouseUp: (e, $scope)->
			navigationPanel = SystemParameters.navigationPanel
			if !navigationPanel then return

			logoutButton = navigationPanel.logoutButton

			if $scope.checkMouseAndHitspotCollision(e,logoutButton.hitspot)
				alert	"logout"


#	-----------------------------  update parameters ------------------
		updateLocation: (data)->
			location = SystemParameters.navigationPanel.location

			mapCircle = data.mapCircle
			cursor = data.cursor

			D = Math.sqrt(Math.pow((mapCircle.x - cursor.x),2)+ Math.pow((mapCircle.y - cursor.y), 2))

			if D <= mapCircle.r
				mapPointer = {
					x:cursor.x
					y:cursor.y
					r:cursor.r
				}
			else
				cursorTmp = {
					x:0
					y:0
				}

				cursorTmp.x = mapCircle.x + (mapCircle.r / D) * (cursor.x - mapCircle.x)
				cursorTmp.y = mapCircle.y + (mapCircle.r / D) * (cursor.y - mapCircle.y)

				mapPointer = {
					x:cursorTmp.x
					y:cursorTmp.y
					r:cursorTmp.r
				}

			locationCircle = location.circle

			location.pointer.x = locationCircle.x - (locationCircle.r / mapCircle.r) * (mapCircle.x - mapPointer.x)
			location.pointer.y = locationCircle.y - (locationCircle.r / mapCircle.r) * (mapCircle.y - mapPointer.y)

			Nvalue = ((38-location.pointer.y)* 90 / 11 ).toFixed(2)
			Eb = 187 - 330 / 75
			Evalue = ( (location.pointer.x - Eb) * 75 / 11 ).toFixed(2)

			location.e.text.text = Evalue
			location.n.text.text = Nvalue

		}
	]