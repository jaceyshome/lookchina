define ['angular', 'appmodule', 'angular_resource'], =>
	appModule = angular.module 'app'
	appModule.factory 'MapAreaPanelControl', ['EVENT','SystemParameters', 'ResourceModel','ShowingPanelsControl','MaximizedPanelControl',(EVENT,SystemParameters,ResourceModel,ShowingPanelsControl,MaximizedPanelControl) =>
		{
		draw: ($scope)->
			canvas = $scope.canvas
			ctx = $scope.context
			if !ctx	then	return

			mapAreaPanel = SystemParameters.mapAreaPanel
			if !mapAreaPanel then return

#			$scope.drawRetangle(mapAreaPanel.hitspot)

#--------------------------------- draw functions ----------------------------

			drawCurrentDynastyResourceIcons = ()->
				if !ResourceModel.currentDynasty or !ResourceModel.currentDynasty.enable then return
				currentDynasty = ResourceModel.currentDynasty
				if(currentDynasty.categories.length == 0) then return

				for category in currentDynasty.categories
					if category.iconName
						index = SystemParameters.categoryPanel.categoryIndexs.indexOf(category.name)
						if !(SystemParameters.categoryPanel.categoryLabels[index]).enable then continue

					for resource in category.resources
						if !resource.iconName then continue
						iconName = 'mapIcon'
						if !resource.icon
							resource.icon = {
								x:resource.x
								y:resource.y
								data:$scope.getImageByName(iconName).data
							}
						$scope.drawImage(resource.icon)

			drawCurrentDynastyResourceIcons()
			ShowingPanelsControl.draw($scope)
			MaximizedPanelControl.draw($scope)
#------------------------------------ end of draw -----------------------------------



#------------------------------------- event handles --------------------------------
		handleMouseUp : (e, $scope)->
			if !ResourceModel.currentDynasty then return
			mapAreaPanel = SystemParameters.mapAreaPanel
			if !mapAreaPanel then return

			if $scope.checkMouseAndHitspotCollision(e,mapAreaPanel.hitspot)
				getResource = false

#				maximized panel collision
				if ResourceModel.showingMaximizedPanel
					if $scope.checkMouseAndHitspotCollision(e,ResourceModel.showingMaximizedPanel.hitspot)
						MaximizedPanelControl.handleMouseUp($scope,e)
						getResource = true
						return

#				showingPanels collision
				if ResourceModel.showingPanels.length > 0
					for showingPanel in ResourceModel.showingPanels
						if $scope.checkMouseAndHitspotCollision(e,showingPanel.hitspot)
							ShowingPanelsControl.handleMouseUp($scope, e, showingPanel)
							getResource = true
							break
							return

#				map icons collision detection
				for category in ResourceModel.currentDynasty.categories
					if getResource then return

					if category.iconName
						index = SystemParameters.categoryPanel.categoryIndexs.indexOf(category.name)
						if !(SystemParameters.categoryPanel.categoryLabels[index]).enable then continue

					for resource in category.resources
						if resource.isShowing then continue

						if !resource.iconName then continue
						if !resource.icon then continue
						hitspot = {
							x: resource.x
							y: resource.y
							width:resource.icon.data.width
							height:resource.icon.data.height
						}
						if $scope.checkMouseAndHitspotCollision(e,hitspot)
							getResource = true
							ShowingPanelsControl.createShowingPanel($scope,resource)
							break
							return
		handleMouseMove:(e, $scope)->
			if $scope.checkMouseAndHitspotCollision(e,SystemParameters.mapAreaPanel.hitspot)
				hitspot = SystemParameters.mapAreaPanel.hitspot

				data = {
					mapCircle:{
						x:hitspot.x + hitspot.width / 2
						y:hitspot.y + hitspot.height
						r:hitspot.width / 2
					}
					cursor:{
						x: e.x
						y: e.y
					}
				}

				$scope.$emit(EVENT.UPDATE_POINTER_POSITION,data)

		}
	]