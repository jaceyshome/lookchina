define ['angular', 'appmodule', 'angular_resource'], =>
	appModule = angular.module 'app'
	appModule.factory 'DynastyMapControl', ['EVENT','SystemParameters', 'ResourceModel',(EVENT,SystemParameters,ResourceModel) =>
		{

		update: ($scope)->

			offsetR = 5

			updateNewMap = ()->

				if SystemParameters.changingMapState.changingNewMap and ResourceModel.newMap?.contentCircle
					contentCircle = ResourceModel.newMap.contentCircle
					outlineCircle = ResourceModel.newMap.outlineCircle

					if contentCircle.r < ResourceModel.newMap.contentCircleConfig.endR
						contentCircle.r += offsetR
						outlineCircle.r = contentCircle.r  + ResourceModel.newMap.outLineWidth

					if contentCircle.r >= ResourceModel.newMap.contentCircleConfig.endR
						SystemParameters.changingMapState.changingNewMap = false


			updateCurrentMap = ()->
				if SystemParameters.changingMapState.changingNewMap then return

				if SystemParameters.changingMapState.changingCurrentMap and ResourceModel.currentMap?.contentCircle
					contentCircle = ResourceModel.currentMap.contentCircle
					outlineCircle = ResourceModel.currentMap.outlineCircle

					if contentCircle.r > ResourceModel.currentMap.contentCircleConfig.startR
						contentCircle.r = contentCircle.r - offsetR
						outlineCircle.r = contentCircle.r + ResourceModel.currentMap.outLineWidth

					if contentCircle.r <= ResourceModel.currentMap.contentCircleConfig.startR
						SystemParameters.changingMapState.changingCurrentMap = false

			syncNewMapAndCurrentMap = ()->

				if !ResourceModel.newMap then return
				if SystemParameters.changingMapState.changingCurrentMap or SystemParameters.changingMapState.changingNewMap then return

				ResourceModel.currentMap = {
					dynasty:ResourceModel.newMap.dynasty
					period:ResourceModel.newMap.period
					mask:ResourceModel.newMap.mask
					outline:ResourceModel.newMap.outline
					contentCircleConfig: ResourceModel.newMap.contentCircleConfig
					outLineWidth:ResourceModel.newMap.outLineWidth
				}

				ResourceModel.currentMap.contentCircle = {
					x: ResourceModel.newMap.contentCircle.x
					y: ResourceModel.newMap.contentCircle.y
					r: ResourceModel.newMap.contentCircle.r
					fillColor: "PANELBG"
					strokeColor: "PANELBG"
					fill:true
					delay:ResourceModel.newMap.contentCircle.delay
				}

				ResourceModel.currentMap.outlineCircle = {
					x: ResourceModel.newMap.contentCircle.x
					y: ResourceModel.newMap.contentCircle.y
					r: ResourceModel.newMap.contentCircle.r + ResourceModel.newMap.outLineWidth
					fillColor: "PANELBG"
					strokeColor: "PANELBG"
					fill:true
					delay:ResourceModel.newMap.contentCircle.delay
				}

				ResourceModel.newMap = undefined

				console.log "swap newMap and CurrentMap"


			updateNewMap()
			updateCurrentMap()
			syncNewMapAndCurrentMap()

		draw: ($scope)->
			ctx = $scope.context
			alphaCtx = $scope.alphaContext
			betaCtx = $scope.betaContext

			if !ctx	then	return
			if !alphaCtx	then	return
			if !betaCtx	then	return

#--------------------------------- draw functions ----------------------------

			drawMap = (map, outline)->
				if map?.contentCircle?
					if outline
						circle = map.outlineCircle
						image = map.outline
					else
						circle = map.contentCircle
						image = map.mask

					alphaCtx.save()
					$scope.clearAlphaContext()
					alphaCtx.drawImage(image.data, 0,0)
					alphaCtx.restore()

					alphaCtx.save()
					alphaCtx.globalCompositeOperation = 'source-in'
					$scope.drawCircle(circle, SystemParameters.ALPHACANVAS)
					alphaCtx.restore()

					betaCtx.save()
					betaCtx.drawImage($scope.alphaCanvas,0,0)
					betaCtx.restore()

			#--------	draw Outline-----------
			isOutLine = true

			betaCtx.save()
			$scope.clearBetaContext()
			drawMap(ResourceModel.currentMap, isOutLine)
			drawMap(ResourceModel.newMap, isOutLine)
			betaCtx.restore()

			#--------- change OutLine color ----------
			alphaCtx.save()
			$scope.clearAlphaContext()
			alphaCtx.drawImage($scope.betaCanvas,0,0)
			alphaCtx.restore()

			alphaCtx.save()
			alphaCtx.globalCompositeOperation = "source-in"
			retangle ={
				x:0
				y:0
				width:$scope.alphaCanvas.width
				height:$scope.alphaCanvas.height
				strokeColor:"SKYBLUE"
				strokeAlpha:0
				fillColor:"SKYBLUE"
				fill:true
			}
			$scope.drawRetangle(retangle, SystemParameters.ALPHACANVAS)
			alphaCtx.restore()

			#-------------- draw outline layer --------------
			ctx.save()
			ctx.drawImage($scope.alphaCanvas,0,0)
			ctx.restore()


			#-------------- draw map ------------
			betaCtx.save()
			$scope.clearBetaContext()
			drawMap(ResourceModel.currentMap)
			drawMap(ResourceModel.newMap)
			betaCtx.restore()

			#--------------- layer outline and map --------------
			ctx.save()
			ctx.globalCompositeOperation = "xor"
			ctx.drawImage($scope.betaCanvas,0,0)
			ctx.globalAlpha = 0.45
			ctx.globalCompositeOperation = "source-over"
			ctx.drawImage($scope.betaCanvas,0,0)
			ctx.globalAlpha = 1
			ctx.restore()






#------------------------------------ end of draw -----------------------------------



#--------------------------------- event handles --------------------------------
		changeCurrentMap: ($scope)->
			dynasty = ResourceModel.currentDynasty
			currentDynastyPeriod = ResourceModel.currentDynastyPeriod

			mapName = dynasty.name + currentDynastyPeriod
			mapOutLineName = dynasty.name + currentDynastyPeriod + "_outline"

			console.log mapName

			ResourceModel.newMap = {
				dynasty:dynasty
				period:currentDynastyPeriod
				mask:$scope.getImageByName(mapName)
				outline:$scope.getImageByName(mapOutLineName)
				contentCircleConfig: dynasty.mapConfigs[currentDynastyPeriod]
				outLineWidth: 3
			}

			ResourceModel.newMap.contentCircle = {
				x: ResourceModel.newMap.contentCircleConfig.startX
				y: ResourceModel.newMap.contentCircleConfig.startY
				r: ResourceModel.newMap.contentCircleConfig.startR
				strokeColor: "PANELBG"
				fillColor: "PANELBG"
				fill:true
				delay:ResourceModel.newMap.contentCircleConfig.delay
			}

			ResourceModel.newMap.outlineCircle = {
				x: ResourceModel.newMap.contentCircleConfig.startX
				y: ResourceModel.newMap.contentCircleConfig.startY
				r: ResourceModel.newMap.contentCircleConfig.startR + ResourceModel.newMap.outLineWidth
				strokeColor: "SKYBLUE"
				fillColor: "SKYBLUE"
				fill:true
				delay:ResourceModel.newMap.contentCircleConfig.delay
			}


			SystemParameters.changingMapState.changingNewMap = true

			if ResourceModel.currentMap
				SystemParameters.changingMapState.changingCurrentMap = true
			else
				SystemParameters.changingMapState.changingCurrentMap = false

		}
	]