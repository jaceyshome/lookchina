define ['angular', 'appmodule', 'angular_resource'], =>
	appModule = angular.module 'app'
	appModule.factory 'CategoryPanelControl', ['EVENT','SystemParameters', 'ResourceModel', (EVENT,SystemParameters, ResourceModel) =>
		return {
		draw: ($scope)->
			canvas = $scope.canvas
			ctx = $scope.context
			if !ctx	then	return

			#-------------------------- property ----------------------------
			categoryPanel = SystemParameters.categoryPanel
			if !categoryPanel then return
			resourceCategories = ResourceModel.resourceCategories
			if !resourceCategories then return
			categoryLabels = categoryPanel.categoryLabels
			if !categoryLabels then return

		#-------------------------- functions -----------------------------
			for i in [0..categoryLabels.length - 1] by 1
				categoryLabel = categoryLabels[i]

				if categoryLabel.enable
					categoryPanel.enableRetangle.y = categoryLabel.y - 12
					categoryPanel.enableRetangle.innerRetangle.y = categoryLabel.y - 8
					$scope.drawRetangle(categoryPanel.enableRetangle)
					$scope.drawRetangle(categoryPanel.enableRetangle.innerRetangle)
					$scope.drawText(categoryLabel)
				else
					categoryPanel.disableRetangle.y = categoryLabel.y - 12
					$scope.drawRetangle(categoryPanel.disableRetangle)
					$scope.drawText(categoryLabel)

#				testing hitspot
				offsetX = -5
				offsetY = -13
				categoryLabel.hitspot = {
					x:categoryPanel.enableRetangle.x + offsetX
					y:categoryLabel.y + offsetY
					width: categoryPanel.panelWidth
					height:16
				}
#				$scope.drawRetangle(categoryLabel.hitspot)

				if i == 0 then startY = categoryLabel.y - 13
				if i == categoryLabels.length - 1
					endY = categoryLabel.y - 13
					offsetY = 5
					offsetHeight = 25
					categoryPanel.hitspot = {
						x: categoryPanel.enableRetangle.x + offsetX
						y: startY - offsetY
						height: endY - startY + offsetHeight
						width: categoryPanel.panelWidth
					}
#					$scope.drawRetangle(categoryPanel.hitspot)


#------------------------------------- event handles --------------------------------
		handleMouseUp: (e, $scope)->
			categoryPanel = SystemParameters.categoryPanel
			if !categoryPanel then return

#			----------------------------------  functions ---------------------------------------
			handleLabelsMouseUpEvent = (e)->
				for label in categoryPanel.categoryLabels
					if $scope.checkMouseAndHitspotCollision(e,label.hitspot)
						label.enable = !label.enable
						$scope.$emit(EVENT.CLEAR_SHOWING_PANELS)
						return
					else
						continue

#			------------------------------------ handle event ------------------------------------

			if $scope.checkMouseAndHitspotCollision(e,categoryPanel.hitspot)
					handleLabelsMouseUpEvent(e)
		}
	]
