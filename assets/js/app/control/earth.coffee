define ['angular', 'appmodule', 'angular_resource', 'threejs', 'stats'], =>
	appModule = angular.module 'app'
	appModule.factory 'EarthControl', ['EVENT','SystemParameters', 'EarthModel', (EVENT,SystemParameters,EarthModel) =>
		{
		init : ($scope)->
			if !$scope.earthContainer then return
			if EarthModel.scene then return

#			width  = window.innerWidth
#			height = window.innerHeight
			width = 1024
			height = 680

#			radius = 0.57
			radius = 0.67
			segments = 64
			rotation = 6

			scene = EarthModel.scene = new THREE.Scene()

			camera = EarthModel.camera = new THREE.PerspectiveCamera(45, width / height, 0.01, 100)
			renderer = EarthModel.renderer = new THREE.WebGLRenderer()


			createSphere = (radius, segments)->
				new THREE.Mesh(
					new THREE.SphereGeometry(radius, segments, segments),
					new THREE.MeshPhongMaterial({
						map:         THREE.ImageUtils.loadTexture('assets/images/2_no_clouds_4k.jpg'),
						bumpMap:     THREE.ImageUtils.loadTexture('assets/images/elev_bump_4k.jpg'),
						bumpScale:   0.000,
						specularMap: THREE.ImageUtils.loadTexture('assets/images/water_4k.png'),
						specular:    new THREE.Color('grey')
					})
				)


			createClouds = (radius, segments)->
				new THREE.Mesh(
					new THREE.SphereGeometry(radius + 0.003, segments, segments),
					new THREE.MeshPhongMaterial({
						map: THREE.ImageUtils.loadTexture('assets/images/fair_clouds_4k.png'),
						transparent: true
					})
				)


			createStars = (radius, segments) ->
				new THREE.Mesh(
					new THREE.SphereGeometry(radius, segments, segments),
					new THREE.MeshBasicMaterial({
						map:  THREE.ImageUtils.loadTexture('assets/images/galaxy_starfield.png'),
						side: THREE.BackSide
					})
				)

			EarthModel.controls = new THREE.TrackballControls(EarthModel.camera);

			renderer.setSize(width, height);
			camera.position.z = 1.5

			scene.add(new THREE.AmbientLight(0x333333))

			light = new THREE.DirectionalLight(0xffffff, 1)
			light.position.set(5,3,5)
			scene.add(light)

			earth = createSphere(radius, segments)
			earth.rotation.y = rotation
			scene.add(earth)

			clouds = createClouds(radius, segments)
			clouds.rotation.y = rotation
			scene.add(clouds)

			stars = createStars(90, 64)
			scene.add(stars)

			EarthModel.light = light
			EarthModel.earth = earth
			EarthModel.clouds = clouds
			EarthModel.stars = stars

			$scope.earthContainer.appendChild(renderer.domElement)

#			EarthModel.earth.position.x -= 0.01
			EarthModel.clouds.position.x -= 2
			EarthModel.earth.rotation.y += 9.3
			EarthModel.earth.rotation.x += 0.62
			EarthModel.earth.rotation.z += 0.2

		update:()->
			undefined
#			EarthModel.controls.update()
#			console.log "x",EarthModel.camera.position.x, "x", EarthModel.camera.position.y, "z", EarthModel.camera.position.z, "rotation:", EarthModel.camera.rotation
#			EarthModel.earth.position.x -= 0.0001
#			EarthModel.clouds.position.x -= 0.0001
#			EarthModel.clouds.rotation.y += 0.00004

		draw: ()->
			if !EarthModel.renderer then return
			EarthModel.renderer.render(EarthModel.scene, EarthModel.camera)
		}
	]