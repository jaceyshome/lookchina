define ['angular', 'appmodule', 'angular_resource'], =>
	appModule = angular.module 'app'
	appModule.factory 'TimelinePanelControl', ['EVENT','SystemParameters', 'ResourceModel', (EVENT,SystemParameters,ResourceModel) =>
		return {
		draw: ($scope)->
			canvas = $scope.canvas
			ctx = $scope.context
			if !ctx	then	return

#-------------------------- property ----------------------------
			timelinePanel = SystemParameters.timelinePanel
			if !timelinePanel then return

#-------------------------- functions -----------------------------

			addMask = ->
				$scope.createRetangleMask(timelinePanel.retangleMask)

			drawProgressionBar = ->
				progressionBar = timelinePanel.progessionBar
				if !progressionBar then return

				drawDynastyProgression = ->
					dynasties = ResourceModel.dynasties
					if !dynasties then return

#					TODO assume current dynastyModel is TANG and poriod is 2
#					ResourceModel.currentDynasty = dynasties[10]
#					ResourceModel.currentDynastyId = dynasties[10].id
#					ResourceModel.currentDynastyPeriod = 1

					drawDynasties = ->
						dynastyModel = timelinePanel.dynastyModel

						dynastyNewHitspots = []
						if !timelinePanel.dynastyHitspots
							timelinePanel.dynastyHitspots = []

						startX = progressionBar.x = timelinePanel.x
						startY = progressionBar.y = timelinePanel.y
						for dynasty in dynasties
							totalPeriod = dynasty.totalPeriod

							startYearText = {
								x:startX
								y:startY - timelinePanel.offset / 2
								color: dynastyModel.startYear.color
								fontWeight:dynastyModel.startYear.fontWeight
								fontFamily:dynastyModel.startYear.fontFamily
								fontSize:dynastyModel.startYear.fontSize
								textAlign:dynastyModel.startYear.textAlign
								text:Math.abs(dynasty.startYear)
							}
							$scope.drawText(startYearText)

							endYearText = {
								x: startYearText.x + dynastyModel.width * totalPeriod + dynastyModel.marginLeft * (totalPeriod - 1)
								y:startY - timelinePanel.offset / 2
								color: dynastyModel.endYear.color
								fontWeight:dynastyModel.endYear.fontWeight
								fontFamily:dynastyModel.endYear.fontFamily
								fontSize:dynastyModel.endYear.fontSize
								textAlign:dynastyModel.endYear.textAlign
								text:Math.abs(dynasty.endYear)
							}
							$scope.drawText(endYearText)

							dynastyName = {
								x:startYearText.x + (endYearText.x - startYearText.x) / 2
								y:startY + dynastyModel.height + timelinePanel.offset
								color: dynastyModel.name.color
								fontWeight:dynastyModel.name.fontWeight
								fontFamily:dynastyModel.name.fontFamily
								fontSize:dynastyModel.name.fontSize
								textAlign:dynastyModel.name.textAlign
								text:dynasty.name
							}
							$scope.drawText(dynastyName)

							drawDynastyPeriod = ->
								dynastyModelInstance =
									x:startX
									y:startY
									fillColor:dynastyModel.fillColor
									strokeColor:dynastyModel.strokeColor
									fillAlpha:dynastyModel.fillAlpha
									strokeAlpha:dynastyModel.strokeAlpha
									width:dynastyModel.width
									height:dynastyModel.height
									marginLeft:dynastyModel.marginLeft
									fill:dynastyModel.fill
								$scope.drawRetangle(dynastyModelInstance)
								return dynastyModelInstance

							drawCurrentDynastyPeriod = ->
								dynastyModelInstance =
									x:startX
									y:startY
									fillColor:timelinePanel.currentDynastyModel.fillColor
									strokeColor:timelinePanel.currentDynastyModel.strokeColor
									fillAlpha:dynastyModel.fillAlpha
									strokeAlpha:dynastyModel.strokeAlpha
									width:dynastyModel.width
									height:dynastyModel.height
									marginLeft:dynastyModel.marginLeft
									fill:dynastyModel.fill
								$scope.drawRetangle(dynastyModelInstance)
								return dynastyModelInstance

							drawPointer = (dynasty)->
								pointer = timelinePanel.pointer
								points = []
								points.push {
										x:dynasty.x + dynasty.width / 2
										y:pointer.y
									}
								points.push {
										x:dynasty.x + dynasty.width / 2 - pointer.width / 2
										y:pointer.y + pointer.height
									}
								points.push {
										x:dynasty.x + dynasty.width / 2 + pointer.width / 2
										y:pointer.y + pointer.height
									}
								pointer.points = points
								$scope.drawPolygon(pointer)

							for i in [0..totalPeriod - 1] by 1
								if dynasty.id == ResourceModel.currentDynastyId && i == ResourceModel.currentDynastyPeriod
									dynastyModelInstance = drawCurrentDynastyPeriod()
									drawPointer(dynastyModelInstance)
								else
									dynastyModelInstance = drawDynastyPeriod()

								startX = dynastyModelInstance.x + dynastyModelInstance.width + dynastyModelInstance.marginLeft

								dynastyNewHitspots.push({
									x:dynastyModelInstance.x
									y:dynastyModelInstance.y
									width:dynastyModelInstance.width
									height:dynastyModelInstance.height
									dynastyId:dynasty.id
									periodId:i
								})

							startX = startX + progressionBar.dyanstyMarginLeft

						timelinePanel.dynastyHitspots = dynastyNewHitspots
#						console.log timelinePanel.dynastyHitspots

					drawDynasties()

				drawDynastyProgression()

			drawTimeLine = ->
				timeLine = timelinePanel.timeLine
				if !timeLine then return
				timeLine.x = timelinePanel.x

				lineBC = timeLine.lineBC
				lineBC.startX = timeLine.x
				lineBC.startY = timeLine.y
				lineBC.endX = timeLine.x + lineBC.width
				lineBC.endY = lineBC.startY
				$scope.drawLine(lineBC)

				textBC = timeLine.textBC
				textBC.x = lineBC.endX + timeLine.textMarginLeft
				textBC.y = lineBC.endY
				$scope.drawText(textBC)

				textDC = timeLine.textDC
				textDC.x = textBC.x + timeLine.textMiddleSpace
				textDC.y = textBC.y
				$scope.drawText(textDC)

				lineDC = timeLine.lineDC
				lineDC.startX = textDC.x + timeLine.textMarginRight
				lineDC.startY = textDC.y
				lineDC.endX = textDC.x + lineDC.width
				lineDC.endY = lineDC.startY
				$scope.drawLine(lineDC)

			endMask = ()->
				$scope.endMask()

#-------------------------------------  execution ---------------------------------
			addMask()
			drawProgressionBar()
			drawTimeLine()
			endMask()


#------------------------------------- event handles --------------------------------
		handleMouseDown: (e, $scope)->
			timelinePanel = SystemParameters.timelinePanel
			if !timelinePanel then return
			if $scope.checkMouseAndHitspotCollision(e,timelinePanel.hitspot)
				timelinePanel.mouseDownCoordinate = $scope.getMousePosition(e)
				timelinePanel.mouseCurrentCoordinate = $scope.getMousePosition(e)
			else
				timelinePanel.mouseCurrentCoordinate = undefined

		handleMouseMove: (e, $scope)->
			timelinePanel = SystemParameters.timelinePanel
			moveRange = timelinePanel.moveRange
			if !timelinePanel then return
			if !timelinePanel.mouseCurrentCoordinate then return

			timelinePanel.mouseMoveCoordinate = $scope.getMousePosition(e)
			moveXDistance = timelinePanel.mouseMoveCoordinate.x - timelinePanel.mouseCurrentCoordinate.x
			timelinePanel.x = timelinePanel.x + moveXDistance
			if timelinePanel.x <= moveRange.min
				timelinePanel.x = moveRange.min
			if timelinePanel.x >= moveRange.max
				timelinePanel.x = moveRange.max
			timelinePanel.mouseCurrentCoordinate = timelinePanel.mouseMoveCoordinate

		handleMouseUp: (e, $scope)->
			timelinePanel = SystemParameters.timelinePanel
			if !timelinePanel then return

			timelinePanel.mouseCurrentCoordinate = undefined

			if $scope.checkMouseAndHitspotCollision(e,timelinePanel.hitspot)

					timelinePanel.mouseUpCoordinate = $scope.getMousePosition(e)
					if Math.abs(timelinePanel.mouseUpCoordinate.x - timelinePanel.mouseDownCoordinate.x) >= 2 or Math.abs(timelinePanel.mouseUpCoordinate.y - timelinePanel.mouseDownCoordinate.y) >= 2
						return

					hitspots = timelinePanel.dynastyHitspots

					for hitspot in hitspots
						if $scope.checkMouseAndHitspotCollision(e,hitspot)
							if ResourceModel.currentDynastyId == hitspot.dynastyId && ResourceModel.currentDynastyPeriod == hitspot.periodId then return
							$scope.$emit(EVENT.ON_CHANGE_CURRENT_DYNASTY, hitspot)
							return
						else
							continue



		}
	]