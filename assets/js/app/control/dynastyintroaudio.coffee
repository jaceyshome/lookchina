define ['angular', 'appmodule', 'angular_resource'], =>
	appModule = angular.module 'app'
	appModule.factory 'DynastyIntroAudioControl', ['EVENT','RemoteIntroAudioService','SystemParameters', 'ResourceModel',(EVENT,RemoteIntroAudioService,SystemParameters,ResourceModel) =>
		{
			watcher:($scope)->
				undefined

			load: ($scope)->
				currentDynasty = ResourceModel.currentDynasty
				if !currentDynasty then return
				if (!currentDynasty.introduction or currentDynasty.introduction == "") then return

				$scope.$emit(EVENT.PLAY_DYNASTY_INTRO_AUDIO, currentDynasty.introAudios[0])

			fadeInText:()->

				if !ResourceModel.currentIntroAudio?.audioId? then return

				ResourceModel.drawingIntroText = {
					audioId:ResourceModel.currentIntroAudio.audioId
					text:ResourceModel.currentIntroAudio.text
					x:SystemParameters.dynastyIntroText.x
					y:SystemParameters.dynastyIntroText.startY
					fontWeight:SystemParameters.dynastyIntroText.fontWeight
					fontFamily:SystemParameters.dynastyIntroText.fontFamily
					fontSize:SystemParameters.dynastyIntroText.fontSize
					color:SystemParameters.dynastyIntroText.color
					textAlign:SystemParameters.dynastyIntroText.textAlign
					width: SystemParameters.dynastyIntroText.width
					alpha:0
					shadow:SystemParameters.dynastyIntroText.shadow
				}

				endY = SystemParameters.dynastyIntroText.endY

				introTextFadeInt = new TWEEN.Tween(ResourceModel.drawingIntroText).to({y:endY, alpha:1}, 500)
					.delay(10)
					.easing(TWEEN.Easing.Quadratic.InOut)
					.onComplete(()->
					).start()


			draw:($scope)->
				if !ResourceModel.drawingIntroText?.audioId? then return
				$scope.drawText(ResourceModel.drawingIntroText)

		}
	]