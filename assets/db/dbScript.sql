SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `lookchina_dev` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `lookchina_dev` ;

-- -----------------------------------------------------
-- Table `lookchina_dev`.`userType`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lookchina_dev`.`userType` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lookchina_dev`.`user`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lookchina_dev`.`user` (
  `id` BIGINT NOT NULL AUTO_INCREMENT ,
  `surName` VARCHAR(45) NULL ,
  `firstName` VARCHAR(45) NULL ,
  `userName` VARCHAR(45) NULL ,
  `dob` TIMESTAMP NULL ,
  `password` VARCHAR(512) NULL ,
  `editDateTime` TIMESTAMP NOT NULL DEFAULT now() ,
  `createDateTime` TIMESTAMP NULL ,
  `userTypeId` INT NOT NULL ,
  PRIMARY KEY (`id`, `userTypeId`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_user_userType_idx` (`userTypeId` ASC) ,
  CONSTRAINT `fk_user_userType`
    FOREIGN KEY (`userTypeId` )
    REFERENCES `lookchina_dev`.`userType` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lookchina_dev`.`resourceType`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lookchina_dev`.`resourceType` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL DEFAULT 'general' ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lookchina_dev`.`dynasty`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lookchina_dev`.`dynasty` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `introduction` VARCHAR(1000) NULL ,
  `startYear` INT NULL ,
  `endYear` INT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lookchina_dev`.`resource`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lookchina_dev`.`resource` (
  `id` BIGINT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(100) NULL ,
  `description` TEXT NULL ,
  `introduction` VARCHAR(1000) NULL ,
  `editDateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `resourceTypeId` INT NOT NULL ,
  `thumbUrl` VARCHAR(512) NULL ,
  `dynastyId` INT NULL ,
  PRIMARY KEY (`id`, `resourceTypeId`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_resource_resourceType1_idx` (`resourceTypeId` ASC) ,
  INDEX `fk_resource_dynasty1_idx` (`dynastyId` ASC) ,
  CONSTRAINT `fk_resource_resourceType1`
    FOREIGN KEY (`resourceTypeId` )
    REFERENCES `lookchina_dev`.`resourceType` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_resource_dynasty1`
    FOREIGN KEY (`dynastyId` )
    REFERENCES `lookchina_dev`.`dynasty` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lookchina_dev`.`userResource`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lookchina_dev`.`userResource` (
  `id` BIGINT NOT NULL AUTO_INCREMENT ,
  `userId` BIGINT NOT NULL ,
  `resourceId` BIGINT NOT NULL ,
  `progress` FLOAT NULL ,
  `editDateTime` TIMESTAMP NULL DEFAULT now() ,
  PRIMARY KEY (`id`, `userId`, `resourceId`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  INDEX `fk_userResource_user1_idx` (`userId` ASC) ,
  INDEX `fk_userResource_resource1_idx` (`resourceId` ASC) ,
  CONSTRAINT `fk_userResource_user1`
    FOREIGN KEY (`userId` )
    REFERENCES `lookchina_dev`.`user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_userResource_resource1`
    FOREIGN KEY (`resourceId` )
    REFERENCES `lookchina_dev`.`resource` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
