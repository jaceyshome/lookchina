-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 05, 2013 at 02:29 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lookchina_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'general',
  `chinese` varchar(45) COLLATE utf8_unicode_ci DEFAULT '普通',
  `iconName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `chinese`, `iconName`) VALUES
(1, 'history', '历史', 'icon_history'),
(2, 'society', '社会', 'icon_society'),
(3, 'culture', '文化', 'icon_culture'),
(4, 'military', '军事', 'icon_military'),
(5, 'technology', '科技', 'icon_technology'),
(6, 'introduction', '介绍', NULL),
(7, 'general', '普通', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dynasty`
--

CREATE TABLE IF NOT EXISTS `dynasty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introduction` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `startYear` int(11) DEFAULT NULL,
  `endYear` int(11) DEFAULT NULL,
  `chinese` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enable` tinyint(1) DEFAULT '0',
  `totalPeriod` int(2) DEFAULT '2',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `dynasty`
--

INSERT INTO `dynasty` (`id`, `name`, `introduction`, `startYear`, `endYear`, `chinese`, `enable`, `totalPeriod`) VALUES
(1, 'xia', NULL, -2070, -1600, '夏', 0, 2),
(2, 'shang', NULL, -1600, -1046, '商', 0, 2),
(3, 'zhou', NULL, -1046, -221, '周', 0, 2),
(4, 'qin', NULL, -221, -206, '秦', 1, 2),
(5, 'han', NULL, -202, 220, '汉', 0, 3),
(6, 'sanguo', NULL, 220, 280, '三国', 0, 2),
(7, 'jin', NULL, 265, 420, '晋', 0, 2),
(8, 'shiliuguo', NULL, 304, 439, '十六国', 0, 2),
(9, 'nanbeichao', NULL, 386, 589, '南北朝', 0, 2),
(10, 'sui', NULL, 581, 618, '隋', 0, 2),
(11, 'tang', NULL, 618, 907, '唐', 1, 4),
(12, 'song', NULL, 960, 1276, '宋', 0, 3),
(13, 'yuan', NULL, 1271, 1368, '元', 0, 3),
(14, 'ming', NULL, 1368, 1644, '明', 0, 3),
(15, 'qing', NULL, 1644, 1911, '清', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `resource`
--

CREATE TABLE IF NOT EXISTS `resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `introduction` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `editDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `resourceCategoryId` int(11) NOT NULL DEFAULT '7',
  `thumbUrl` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dynastyId` int(11) DEFAULT NULL,
  `x` float DEFAULT NULL,
  `y` float DEFAULT NULL,
  `z` float DEFAULT NULL,
  `resourceTypeId` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_resource_dynasty1_idx` (`dynastyId`),
  KEY `fk_resource_resourceCategory_idx` (`resourceCategoryId`),
  KEY `fk_resource_resourceTypeid_idx` (`resourceTypeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=49 ;

--
-- Dumping data for table `resource`
--

INSERT INTO `resource` (`id`, `name`, `description`, `introduction`, `editDateTime`, `resourceCategoryId`, `thumbUrl`, `dynastyId`, `x`, `y`, `z`, `resourceTypeId`) VALUES
(1, 'Qin Dynasty introduction', 'The Qin Dynasty was the first imperial dynasty of China, lasting from 221 to 206 BC. The dynasty was formed after the conquest of the six other states by the state of Qin, and its founding emperor was known as Qin Shi Huang, the First Emperor of Qin. The Qin state derived its name from its heartland of Qin, in modern-day Gansu and Shaanxi. The strength of the Qin state was greatly increased by the legalist reforms of Shang Yang in the 4th century BC, during the Warring States period. In the mid and late third century BC, the Qin accomplished a series of swift conquests, first ending the powerless Zhou Dynasty, and eventually conquering the remaining six states of the major states to gain control over the whole of China, resulting in a unified China.', NULL, '2013-10-09 11:50:08', 6, NULL, 4, 390, 420, NULL, 1),
(2, 'Origins and early development', 'In the 9th century BC Feizi, a descendant of the ancient political advisor Gao Yao, was granted rule over Qin City. The modern city of Tianshui stands where this city once was. During the rule of King Xiao of Zhou, the eighth king of the Zhou Dynasty, this area became known as the state of Qin. In 897 BC, under the regency of Gonghe, the area became a dependency allotted for the purpose of raising and breeding horses. One of Feizi''s descendants, Duke Zhuang, became favoured by King Ping of Zhou, the thirteenth king in that line. As a reward, Zhuang''s son, Duke Xiang, was sent eastward as the leader of a war expedition, during which he formally established the Qin. \r\nQin state first sent a military expedition into central China in 672 BC, though it did not engage in any serious incursions due to the threat from neighbouring tribesmen. By the dawn of the fourth century BC, however, the neighbouring tribes had all been either subdued or conquered, and the stage was set for the rise of Qin expansionism. ', NULL, '2013-10-09 11:51:28', 1, NULL, 4, 400, 400, NULL, 1),
(8, 'Growth of power', 'Lord Shang Yang, a Qin statesman, introduced a number of militarily advantageous reforms from 361 BC until his death in 338 BC, and also helped construct the Qin capital, Xianyang. This latter accomplishment commenced in the mid-fourth century BC; the resulting city greatly resembled the capitals of other Warring States.', NULL, '2013-10-09 11:52:50', 1, NULL, 4, 200, 300, NULL, 1),
(9, 'Conquest of the Warring States', 'During the Warring States Period preceding the Qin Dynasty, the major states vying for dominance were Yan, Zhao, Qi, Chu, Han, Wei and Qin. The rulers of these states styled themselves as kings, rather than using the titles of lower nobility they had previously held. However, none elevated himself to believe that he had the "Mandate of Heaven," as the Zhou emperors had claimed, nor that he had the right to offer sacrifices—they left this to the Zhou rulers.', NULL, '2013-10-09 13:35:14', 1, NULL, 4, 300, 200, NULL, 1),
(10, 'Southward expansion', 'In 214 BC Qin Shihuang secured his boundaries to the north with a fraction (100,000 men) of his large army, and sent the majority (500,000 men) of his army south to conquer the territory of the southern tribes. Prior to the events leading to Qin dominance over China, they had gained possession of much of Sichuan to the southwest. The Qin army was unfamiliar with the jungle terrain, and it was defeated by the southern tribes'' guerrilla warfare tactics with over 100,000 men lost. However, in the defeat Qin was successful in building a canal to the south, which they used heavily for supplying and reinforcing their troops during their second attack to the south. Building on these gains, the Qin armies conquered the coastal lands surrounding Guangzhou, and took the provinces of Fuzhou and Guilin. They struck as far south as Hanoi. After these victories in the south, Qin Shihuang moved over 100,000 prisoners and exiles to colonize the newly conquered area. In terms of extending the boundaries of his empire, the First Emperor was extremely successful in the south.', NULL, '2013-10-09 13:35:17', 1, NULL, 4, 500, 400, NULL, 1),
(11, 'Domestic life', 'The aristocracy of the Qin were largely similar in their culture and daily life. Regional variations in culture were considered a symbol of the lower classes. This stemmed from the Zhou and was seized upon by the Qin, as such variations were seen as contrary to the unification that the government strove to achieve.', NULL, '2013-10-09 13:38:18', 2, NULL, 4, 450, 300, NULL, 1),
(12, 'Architecture', 'Warring States-era architecture had several definitive aspects. City walls, used for defense, were made longer, and indeed several secondary walls were also sometimes built to separate the different districts. Versatility in federal structures was emphasized, to create a sense of authority and absolute power. Architectural elements such as high towers, pillar gates, terraces, and high buildings amply conveyed this.[', NULL, '2013-10-09 13:39:54', 2, NULL, 4, 350, 150, NULL, 1),
(13, 'Government and military', 'The Qin government was highly bureaucratic, and was administered by a hierarchy of officials, all serving the First Emperor. The Qin put into practice the teachings of Han Fei, allowing the First Emperor to control all of his territories, including those recently conquered. All aspects of life were standardized, from measurements and language to more practical details, such as the length of chariot axles. Zheng and his advisers also introduced new laws and practices that ended feudalism in China, replacing it with a centralized, bureaucratic government. Under this system, both the military and government thrived, as talented individuals could be more easily identified in the transformed society. Later Chinese dynasties emulated the Qin government for its efficiency, despite its being condemned by Confucian philosophy.[15][36] Such a system, however, could be manipulated by power-hungry individuals; one example of such an occurrence was documented in the "Records of Officialdom". A commander named Hu ordered his men to attack peasants, in an attempt to increase the number of "bandits" he had killed; his superiors, likely eager to inflate their records as well, allowed this.', NULL, '2013-10-09 13:40:59', 4, NULL, 4, 250, 550, NULL, 1),
(14, 'Religion', 'The dominant religious belief in China during the reign of the Qin, and, in fact, during much of early imperial China, was focused on the shen (roughly translating to "spirits"), yin ("shadows"), and the realm they were said to live in. The Chinese offered sacrifices in an attempt to contact this other world, which they believed to be parallel to the earthly one. The dead were said to simply have moved from one world to the other. The rituals mentioned, as well as others, served two purposes: to ensure that the dead journeyed and stayed in the other realm, and to receive blessings from the spirit realm.', NULL, '2013-10-09 13:42:04', 3, NULL, 4, 320, 230, NULL, 1),
(16, 'Philosophy and literature', 'During the Warring States Period, the Hundred Schools of Thought comprised many different philosophies proposed by Chinese scholars. In 221 BC, however, the First Emperor conquered all of the states and governed with a single philosophy, Legalism. At least one school of thought, Mohism, was eradicated, though the reason is not known. Despite the Qin''s state ideology and Mohism being similar in certain regards, it is possible that Mohists were sought and killed by the state''s armies due to paramilitary activities.', NULL, '2013-10-09 13:43:19', 3, NULL, 4, 280, 530, NULL, 1),
(17, 'Inventions', 'Legalism\r\nStandardized writing and language\r\nStandardized money\r\nStandardized system of measurement\r\nIrrigation projects\r\nBuilding of the Great Wall\r\nTerra cotta army\r\nExpanded Network of Roads and Canals\r\nMultiplication Table', NULL, '2013-10-09 13:46:02', 5, NULL, 4, 420, 400, NULL, 1),
(18, 'Introduction', 'The Tang Dynasty (618 – 907 AD) was an imperial dynasty of China preceded by the Sui Dynasty and followed by the Five Dynasties and Ten Kingdoms period. It was founded by the Li family, who seized power during the decline and collapse of the Sui Empire. The dynasty was interrupted briefly by the Second Zhou Dynasty (October 8, 690 – March 3, 705) when Empress Wu Zetian seized the throne, becoming the only Chinese empress regnant to rule in her own right.', NULL, '2013-10-10 09:32:02', 6, NULL, 11, NULL, NULL, NULL, 1),
(19, 'Establishment', 'The Li family belonged to the northwest military aristocracy prevalent during the reign of the Sui emperors. The mothers of both Emperor Yang of Sui (r. 604–617) and the founding emperor of Tang were sisters, making these two emperors of different dynasties first cousins. Li Yuan (later to become Emperor Gaozu of Tang, r. 618–626) was the Duke of Tang and former governor of Taiyuan when other government officials were fighting off bandit leaders in the collapse of the Sui Empire, caused in part by a failed Korean campaign. With prestige and military experience, he later rose in rebellion along with his son and his equally militant daughter Princess Pingyang (d. 623) who raised her own troops and commanded them. In 617, Li Yuan occupied Chang''an and acted as regent over a puppet child emperor of the Sui, relegating Emperor Yang to the position of Taishang Huang, or retired emperor/father of the present emperor. With the news of Emperor Yang''s murder by his general Yuwen Huaji (d. 619), on June 18, 618, Li Yuan declared himself the emperor of a new dynasty, the Tang.', NULL, '2013-10-10 09:33:52', 1, NULL, 11, 320, 120, NULL, 1),
(20, 'Initial reforms', 'Taizong set out to solve internal problems within the government which had constantly plagued past dynasties. Building upon the Sui legal code, he issued a new legal code that subsequent Chinese dynasties would model theirs upon, as well as neighboring polities in Vietnam, Korea, and Japan. The earliest law code to survive though was the one established in the year 653, which was divided into 500 articles specifying different crimes and penalties ranging from ten blows with a light stick, one hundred blows with a heavy rod, exile, penal servitude, or execution. The legal code clearly distinguished different levels of severity in meted punishments when different members of the social and political hierarchy committed the same crime. For example, the severity of punishment was different when a servant or nephew killed a master or an uncle than when a master or uncle killed a servant or nephew. The Tang Code was largely retained by later codes such as the early Ming Dynasty (1368–1644) code of 1397, yet there were several revisions in later times, such as improved property rights for women during the Song Dynasty', NULL, '2013-10-10 09:36:01', 1, NULL, 11, 450, 340, NULL, 1),
(21, 'Imperial examinations', 'Following the Sui Dynasty''s example, the Tang abandoned the nine-rank system in favor of a service system. Students of Confucian studies were potential candidates for the imperial examinations, the graduates of which could be appointed as state bureaucrats in the local, provincial, and central government. There were two types of exams that were given, mingjing (''illuminating the classics examination'') and jinshi (''presented scholar examination'').  The mingjing was based upon the Confucian classics and tested the student''s knowledge of a broad variety of texts. The jinshi tested a student''s literary abilities in writing essay-style responses to questions on matters of governance and politics, as well as their skills in composing poetry. Candidates were also judged on their skills of deportment, appearance, speech, and level of skill in calligraphy, all of which were subjective criteria that allowed the already wealthy members of society to be chosen over ones of more modest means who were unable to be educated in rhetoric or fanciful writing skills. There was a disproportionate number of civil officials coming from aristocratic as opposed to non-aristocratic families. The exams were open to all male subjects whose fathers were not of the artisan or merchant classes, although having wealth or noble status was not a prerequisite in receiving a recommendation. In order to promote widespread Confucian education, the Tang government established state-run schools and issued standard versions of the Five Classics with selected commentaries.', NULL, '2013-10-10 09:37:28', 1, NULL, 11, 560, 120, NULL, 1),
(22, 'Religion and politics', 'From the outset, religion played a role in Tang politics. In his bid for power, Li Yuan had attracted a following by claiming descent from the Daoist sage Laozi (fl. 6th century BC). People bidding for office would have monks from Buddhist temples pray for them in public in return for cash donations or gifts if the person was selected. Before the persecution of Buddhism in the 9th century, Buddhism and Daoism were accepted side by side, and Emperor Xuanzong of Tang (r. 712–56) invited monks and clerics of both religions to his court. At the same time Xuanzong exalted the ancient Laozi by granting him grand titles, wrote commentary on the Daoist Laozi, set up a school to prepare candidates for examinations on Daoist scriptures, and called upon the Indian monk Vajrabodhi (671–741) to perform Tantric rites to avert a drought in the year 726. In 742 Emperor Xuanzong personally held the incense burner during the ceremony of the Ceylonese monk Amoghavajra (705–74) reciting "mystical incantations to secure the victory of Tang forces." While religion played a role in politics, politics also played a role in religion. In the year 714, Emperor Xuanzong forbade shops and vendors in the city of Chang''an to sell copied Buddhist sutras, instead giving the Buddhist clergy of the monasteries the sole right to distribute sutras to the laity. In the previous year of 713, Emperor Xuanzong had liquidated the highly lucrative Inexhaustible Treasury, which was run by a prominent Buddhist monastery in Chang''an. This monastery collected vast amounts of money, silk, and treasures through multitudes of anonymous people''s repentances, leaving the donations on the monastery''s premise. Although the monastery was generous in donations, Emperor Xuanzong issued a decree abolishing their treasury on grounds that their banking practices were fraudulent, collected their riches, and distributed the wealth to various other Buddhist monasteries, Daoist abbeys, and to repair statues, halls, and bridges in the city.', NULL, '2013-10-10 09:38:45', 1, NULL, 11, 220, 670, NULL, 1),
(23, 'Taxes and the census', 'The Tang Dynasty government attempted to create an accurate census of the size of their empire''s population, mostly for effective taxation and matters of military conscription for each region. The early Tang government established both the grain tax and cloth tax at a relatively low rate for each household under the empire. This was meant to encourage households to enroll for taxation and not avoid the authorities, thus providing the government with the most accurate estimate possible. In the census of 609, the population was tallied by efforts of the government at a size of 9 million households, or about 50 million people. The Tang census of 742 again approximated the size of China''s population at about 50 million people. Patricia Ebrey writes that even if a rather significant number of people had avoided the registration process of the tax census, the population size during the Tang had not grown significantly since the earlier Han Dynasty (the census of the year 2 recording a population of roughly 58 million people in China). S.A.M. Adshead disagrees, estimating that there were about 75 million people by 750', NULL, '2013-10-10 09:40:11', 1, NULL, 11, 320, 476, NULL, 1),
(24, 'Protectorates and tributaries', 'The 7th century and first half of the 8th century is generally considered the zenith era of the Tang Dynasty. Emperor Tang Xuanzong brought the Middle Kingdom to its golden age while the Silk Road thrived, with sway over Indochina in the south, and to the west Tang China was master of the Pamirs (modern-day Tajikistan) and protector of Kashmir bordering Persia. \r\nSome of the kingdoms paying tribute to the Tang Dynasty included Kashmir, Nepal, Khotan, Kucha, Kashgar, Japan, Korea, Champa, and kingdoms located in Amu Darya and Syr Darya valley.  Turkic nomads addressed the Emperor of Tang China as Tian Kehan.  After the widespread Göktürk revolt of Shabolüe Khan (d. 658) was put down at Issyk Kul in 657 by Su Dingfang (591–667), Emperor Gaozong established several protectorates governed by a Protectorate General or Grand Protectorate General, which extended the Chinese sphere of influence as far as Herat in Western Afghanistan. Protectorate Generals were given a great deal of autonomy to handle local crises without waiting for central admission. After Xuanzong''s reign, military governors (jiedushi) were given enormous power, including the ability to maintain their own armies, collect taxes, and pass their titles on hereditarily. This is commonly recognized as the beginning of the fall of Tang''s central government.', NULL, '2013-10-10 09:41:15', 4, NULL, 11, 430, 230, NULL, 1),
(25, 'Soldiers and conscription', 'By the year 737, Emperor Xuanzong discarded the policy of conscripting soldiers that were replaced every three years, replacing them with long-service soldiers who were more battle-hardened and efficient.[54] It was more economically feasible as well, since training new recruits and sending them out to the frontier every three years drained the treasury.[54] By the late 7th century, the fubing troops began abandoning military service and the homes provided to them in the equal-field system. The supposed standard of 100 mu of land allotted to each family was in fact decreasing in size in places where population expanded and the wealthy bought up most of the land.[55] Hard-pressed peasants and vagrants were then induced into military service with benefits of exemption from both taxation and corvée labor service, as well as provisions for farmland and dwellings for dependents who accompanied soldiers on the frontier.[56] By the year 742 the total number of enlisted troops in the Tang armies had risen to about 500,000 men.', NULL, '2013-10-10 09:42:05', 4, NULL, 11, 390, 270, NULL, 1),
(26, 'Turkic and Western regions', 'The Sui and Tang carried out very successful military campaigns against the steppe nomads. Chinese foreign policy to the north and west now had to deal with Turkic nomads, who were becoming the most dominant ethnic group in Central Asia.[58][59] To handle and avoid any threats posed by the Turks, the Sui government repaired fortifications and received their trade and tribute missions.[29] They sent royal princesses off to marry Turkic clan leaders, a total of four of them in 597, 599, 614, and 617. The Sui stirred trouble and conflict amongst ethnic groups against the Turks.[60][61] As early as the Sui Dynasty, the Turks had become a major militarized force employed by the Chinese. When the Khitans began raiding northeast China in 605, a Chinese general led 20,000 Turks against them, distributing Khitan livestock and women to the Turks as a reward.[62] On two occasions between 635 to 636, Tang royal princesses were married to Turk mercenaries or generals in Chinese service.[61] Throughout the Tang Dynasty until the end of 755, there were approximately ten Turkic generals serving under the Tang.[63][64] While most of the Tang army was made of fubing Chinese conscripts, the majority of the troops led by Turkic generals were of non-Chinese origin, campaigning largely in the western frontier where the presence of fubing troops was low.[65] Some "Turkic" troops were nomadisized Han Chinese, a desinicized people.', NULL, '2013-10-10 09:43:11', 2, NULL, 11, 720, 360, NULL, 1),
(27, 'Silk Road', 'The Silk Road was the most important pre-modern Eurasian trade route. During this period of the Pax Sinica, the Silk Road reached its golden age, whereby Persian and Sogdian merchants benefited from the commerce between East and West. At the same time, the Chinese empire welcomed foreign cultures making it very cosmopolitan in its urban centers.', NULL, '2013-10-10 09:44:08', 2, NULL, 11, 680, 220, NULL, 1),
(28, 'Seaports and maritime trade', 'Chinese envoys had been sailing through the Indian Ocean to India since perhaps the 2nd century BC,  yet it was during the Tang Dynasty that a strong Chinese maritime presence could be found in the Persian Gulf and Red Sea, into Persia, Mesopotamia (sailing up the Euphrates River in modern-day Iraq), Arabia, Egypt, Aksum (Ethiopia), and Somalia in the Horn of Africa. From the same Quraysh tribe of Muhammad, Sa''d ibn Abi-Waqqas sailed from Ethiopia to China during the reign of Emperor Gaozu. He later traveled back to China with a copy of the Quran, establishing China''s first mosque, the Mosque of Remembrance, during the reign of Emperor Gaozong. To this day he is still buried in a Muslim cemetery at Guangzhou.', NULL, '2013-10-10 09:45:17', 3, NULL, 11, 360, 240, NULL, 1),
(29, 'An Shi Rebellion and catastrophe', 'he Tang Empire was at its height of power up until the middle of the 8th century, when the An Shi Rebellion (December 16, 755 – February 17, 763) destroyed the prosperity of the empire. An Lushan was a half-Sogdian, half-Turk Tang commander since 744, had experience fighting the Khitans of Manchuria with a victory in 744,[52][150] yet most of his campaigns against the Khitans were unsuccessful.[151] He was given great responsibility in Hebei, which allowed him to rebel with an army of more than one hundred thousand troops.[52] After capturing Luoyang, he named himself emperor of a new, but short-lived, Yan state.[150] Despite early victories scored by Tang General Guo Ziyi (697–781), the newly recruited troops of the army at the capital were no match for An Lushan''s die-hard frontier veterans, so the court fled Chang''an.[52] While the heir apparent raised troops in Shanxi and Xuanzong fled to Sichuan province, they called upon the help of the Uyghur Turks in 756.[152] The Uyghur khan Moyanchur was greatly excited at this prospect, and married his own daughter to the Chinese diplomatic envoy once he arrived, receiving in turn a Chinese princess as his bride.[152] The Uyghurs helped recapture the Tang capital from the rebels, but they refused to leave until the Tang paid them an enormous sum of tribute in silk.[52][152] Even Abbasid Arabs assisted the Tang in putting down An Lushan''s rebellion.[152][153] The Tibetans took hold of the opportunity and raided many areas under Chinese control, and even after the Tibetan Empire had fallen apart in 842 (and the Uyghurs soon after) the Tang were in no position to reconquer Central Asia after 763.[52][154] So significant was this loss that half a century later jinshi examination candidates were required to write an essay on the causes of the Tang''s decline.[155] Although An Lushan was killed by one of his eunuchs in 757,[152] this time of troubles and widespread insurrection continued until rebel Shi Siming was killed by his own son in 763.', NULL, '2013-10-10 09:47:36', 3, NULL, 11, 320, 650, NULL, 1),
(30, 'Leisure in the Tang', 'Much more than earlier periods, the Tang era was renowned for the time reserved for leisure activity, especially for those in the upper classes.[169] Many outdoor sports and activities were enjoyed during the Tang, including archery,[170] hunting,[171] horse polo,[172] cuju football,[173] cockfighting,[174] and even tug of war.[175] Government officials were granted vacations during their tenure in office. Officials were granted 30 days off every three years to visit their parents if they lived 1,000 mi (1,600 km) away, or 15 days off if the parents lived more than 167 mi (269 km) away (travel time not included).[169] Officials were granted nine days of vacation time for weddings of a son or daughter, and either five, three, or one days/day off for the nuptials of close relatives (travel time not included).[169] Officials also received a total of three days off for their son''s capping initiation rite into manhood, and one day off for the ceremony of initiation rite of a close relative''s son.', NULL, '2013-10-10 09:49:49', 7, NULL, NULL, NULL, NULL, NULL, 1),
(31, 'Chang''an, the Tang capital', 'Although Chang''an was the capital of the earlier Han and Jin dynasties, after subsequent destruction in warfare, it was the Sui Dynasty model that comprised the Tang era capital. The roughly square dimensions of the city had six miles (10 km) of outer walls running east to west, and more than five miles (8 km) of outer walls running north to south.[16] The royal palace, the Taiji Palace, stood north of the city''s central axis.[181] From the large Mingde Gates located mid-center of the main southern wall, a wide city avenue stretched from there all the way north to the central administrative city, behind which was the Chentian Gate of the royal palace, or Imperial City. Intersecting this were fourteen main streets running east to west, while eleven main streets ran north to south. These main intersecting roads formed 108 rectangular wards with walls and four gates each, and each ward filled with multiple city blocks. The city was made famous for this checkerboard pattern of main roads with walled and gated districts, its layout even mentioned in one of Du Fu''s poems.[182] During the Heian period, the city of Heian kyō (present-day Kyoto) of Japan like many cities was arranged in the checkerboard street grid pattern of the Tang capital and in accordance with traditional geomancy following the model of Chang''an.[29] Of these 108 wards in Chang''an, two of them (each the size of two regular city wards) were designated as government-supervised markets, and other space reserved for temples, gardens, ponds, etc.[16] Throughout the entire city, there were 111 Buddhist monasteries, 41 Daoist abbeys, 38 family shrines, 2 official temples, 7 churches of foreign religions, 10 city wards with provincial transmission offices, 12 major inns, and 6 graveyards.[183] Some city wards were literally filled with open public playing fields or the backyards of lavish mansions for playing horse polo and cuju football.[184] In 662, Emperor Gaozong moved the imperial court to the Daming Palace, which became the political center of the empire and served as the royal residence of the Tang emperors for more than 220 years.', NULL, '2013-10-10 09:51:25', 3, NULL, 11, 430, 500, NULL, 1),
(32, 'Literature', 'The Tang period was a golden age of Chinese literature and art. There are over 48,900 poems penned by some 2,200 Tang authors that have survived until modern times.[194][195] Skill in the composition of poetry became a required study for those wishing to pass imperial examinations,[196] while poetry was also heavily competitive; poetry contests amongst guests at banquets and courtiers were common.[197] Poetry styles that were popular in the Tang included gushi and jintishi, with the renowned poet Li Bai (701–762) famous for the former style, and poets like Wang Wei (701–761) and Cui Hao (704–754) famous for their use of the latter. Jintishi poetry, or regulated verse, is in the form of eight-line stanzas or seven characters per line with a fixed pattern of tones that required the second and third couplets to be antithetical (although the antithesis is often lost in translation to other languages).[198] Tang poems remained popular and great emulation of Tang era poetry began in the Song Dynasty; in that period, Yan Yu (嚴羽; active 1194–1245) was the first to confer the poetry of the High Tang (c. 713–766) era with "canonical status within the classical poetic tradition." Yan Yu reserved the position of highest esteem among all Tang poets for Du Fu (712–770), who was not viewed as such in his own era, and was branded by his peers as an anti-traditional rebel.', NULL, '2013-10-10 09:52:12', 3, NULL, 11, 290, 390, NULL, 1),
(33, 'Religion and philosophy', 'Since ancient times, the Chinese believed in a folk religion that incorporated many deities. The Chinese believed that the afterlife was a reality parallel to the living world, complete with its own bureaucracy and afterlife currency needed by dead ancestors.[211] Funerary practices included providing the deceased with everything they might need in the afterlife, including animals, servants, entertainers, hunters, homes, and officials. This ideal is reflected in Tang dynasty art.[111] This is also reflected in many short stories written in the Tang about people accidentally winding up in the realm of the dead, only to come back and report their experiences.', NULL, '2013-10-10 09:52:54', 3, NULL, 11, 230, 450, NULL, 1),
(34, 'Engineering', 'Technology during the Tang period was built also upon the precedents of the past. Advancements in clockworks and timekeeping included the mechanical gear systems of Zhang Heng (78–139) and Ma Jun (fl. 3rd century) gave the Tang engineer, astronomer, and monk Yi Xing (683–727) inspiration when he invented the world''s first clockwork escapement mechanism in 725.[249] This was used alongside a clepsydra clock and waterwheel to power a rotating armillary sphere in representation of astronomical observation.[250] Yi Xing''s device also had a mechanically timed bell that was struck automatically every hour, and a drum that was struck automatically every quarter hour; essentially, a striking clock.[251] Yi Xing''s astronomical clock and water-powered armillary sphere became well known throughout the country, since students attempting to pass the imperial examinations by 730 had to write an essay on the device as an exam requirement.[252] However, the most common type of public and palace timekeeping device was the inflow clepsydra. Its design was improved c. 610 by the Sui-dynasty engineers Geng Xun and Yuwen Kai. They provided a steelyard balance that allowed seasonal adjustment in the pressure head of the compensating tank and could then control the rate of flow for different lengths of day and night.', NULL, '2013-10-10 09:53:48', 5, NULL, 11, 320, 400, NULL, 1),
(35, 'Woodblock printing', 'Woodblock printing made the written word available to vastly greater audiences. One of the world''s oldest surviving printed documents is a miniature Buddhist dharani sutra unearthed at Xi''an in 1974 and dated roughly from 650 to 670.[260] The Diamond Sutra is the first full-length book printed at regular size, complete with illustrations embedded with the text and dated precisely to 868.[261][262] Among the earliest documents to be printed were Buddhist texts as well as calendars, the latter essential for calculating and marking which days were auspicious and which days were not.[263] With so many books coming into circulation for the general public, literacy rates could improve, along with the lower classes being able to obtain cheaper sources of study. Therefore, there were more lower-class people seen entering the Imperial Examinations and passing them by the later Song Dynasty.[33][264][265] Although the later Bi Sheng''s movable type printing in the 11th century was innovative for his period, woodblock printing that became widespread in the Tang would remain the dominant printing type in China until the more advanced printing press from Europe became widely accepted and used in East Asia.[266] The first use of the playing card during the Tang Dynasty was an auxiliary invention of the new age of printing.', NULL, '2013-10-10 09:54:30', 5, NULL, 11, 630, 390, NULL, 1),
(36, 'Medicine', 'The Chinese of the Tang era were also very interested in the benefits of officially classifying all of the medicines used in pharmacology. In 657, Emperor Gaozong of Tang (r. 649–683) commissioned the literary project of publishing an official materia medica, complete with text and illustrated drawings for 833 different medicinal substances taken from different stones, minerals, metals, plants, herbs, animals, vegetables, fruits, and cereal crops.[268] In addition to compiling pharmacopeias, the Tang fostered learning in medicine by upholding imperial medical colleges, state examinations for doctors, and publishing forensic manuals for physicians.[245] Authors of medicine in the Tang include Zhen Chuan (d. 643) and Sun Simiao (581–682), the former who first identified in writing that patients with diabetes had an excess of sugar in their urine, and the latter who was the first to recognize that diabetic patients should avoid consuming alcohol and starchy foods.[269] As written by Zhen Chuan and others in the Tang, the thyroid glands of sheep and pigs were successfully used to treat goiters; thyroid extracts were not used to treat patients with goiter in the West until 1890.[270] The use of the dental amalgam, manufactured from tin and silver, was first introduced in the medical text Hsin Hsiu Pen Tsao written by Su Kung in 659.', NULL, '2013-10-10 09:55:18', 5, NULL, 11, 580, 230, NULL, 1),
(37, 'Cartography', 'In the realm of cartography, there were further advances beyond the map-makers of the Han Dynasty. When the Tang chancellor Pei Ju (547–627) was working for the Sui Dynasty as a Commercial Commissioner in 605, he created a well-known gridded map with a graduated scale in the tradition of Pei Xiu (224–271).[273] The Tang chancellor Xu Jingzong (592–672) was also known for his map of China drawn in the year 658.[274] In the year 785 the Emperor Dezong had the geographer and cartographer Jia Dan (730–805) complete a map of China and her former colonies in Central Asia.[274] Upon its completion in 801, the map was 9.1 m (30 ft) in length and 10 m (33 ft) in height, mapped out on a grid scale of one inch equaling one hundred li (Chinese unit of measuring distance).[274] A Chinese map of 1137 is similar in complexity to the one made by Jia Dan, carved on a stone stele with a grid scale of 100 li.[275] However, the only type of map that has survived from the Tang period are star charts. Despite this, the earliest extant terrain maps of China come from the ancient State of Qin; maps from the 4th century BC that were excavated in 1986.', NULL, '2013-10-10 09:56:14', 5, NULL, 11, 300, 460, NULL, 2),
(38, 'Historiography', 'The first classic work about the Tang is the Book of Tang by Liu Xu (887–946 AD) et al. of the Later Jin, who redacted it during the last years of his life. This was edited into another history (labelled the New Book of Tang) in order to distinguish it, which was a work by the Song historians Ouyang Xiu (1007–1072), Song Qi (998–1061), et al. of the Song Dynasty (between the years 1044 and 1060). Both of them were based upon earlier annals, yet those are now lost.[284] Both of them also rank among the Twenty-Four Histories of China. One of the surviving sources of the Book of Tang, primarily covering up to 756, is the Tongdian, which Du You presented to the emperor in 801. The Tang period was again placed into the enormous universal history text of the Zizhi Tongjian, edited, compiled, and completed in 1084 by a team of scholars under the Song Dynasty Chancellor Sima Guang (1019–1086). This historical text, written with 3 million Chinese characters in 294 volumes, covered the history of China from the beginning of the Warring States (403 BC) until the beginning of the Song Dynasty (960)', NULL, '2013-10-10 09:59:09', 5, NULL, 11, 840, 230, NULL, 1),
(39, 'The First Emperor: The Man Who Made China (Part 3)', 'assets/videos/apple.webm', NULL, '2013-10-10 10:31:36', 1, NULL, 4, 390, 480, NULL, 3),
(40, 'Qin Dynasty Engineering_an_Empire_-_China', 'assets/videos/banana.webm', NULL, '2013-10-10 10:32:56', 4, NULL, 4, 248, 498, NULL, 3),
(41, 'Qin Dynasty 1/2', 'assets/videos/melon.webm', NULL, '2013-10-10 10:34:24', 2, NULL, 4, 280, 430, NULL, 3),
(42, 'Daming Palace', 'assets/videos/orange.webm', NULL, '2013-10-10 10:40:17', 1, NULL, 11, 372, 230, NULL, 3),
(43, 'Tang Dynasty Landscape Painting', 'assets/videos/peach.webm', NULL, '2013-10-10 10:40:45', 7, NULL, 11, 350, 290, NULL, 3),
(44, 'The Tang Dynasty', 'assets/videos/pieApple.webm', NULL, '2013-10-10 10:42:10', 7, NULL, 11, 420, 378, NULL, 3),
(45, 'Emperor Taizong gives an audience to the ambassador of Tibet', 'assets/data/images/800px-Emperor_Taizong.jpg', NULL, '2013-10-10 10:46:18', 2, NULL, 11, 520, 350, NULL, 2),
(46, 'Gilt silver eared cup', 'assets/data/images/Gilt_silver_eared_cup.jpg', NULL, '2013-10-10 10:47:02', 3, NULL, 11, 630, 420, NULL, 2),
(47, 'Terracotta Army-China2', 'assets/data/images/800px-Terracotta_Army-China2.jpg', NULL, '2013-10-10 11:19:32', 4, NULL, 4, 350, 400, NULL, 2),
(48, 'Departure Herald-Detail', 'assets/data/images/Departure_Herald-Detail.jpg', NULL, '2013-10-10 11:21:31', 3, NULL, 4, 500, 290, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `resourcetype`
--

CREATE TABLE IF NOT EXISTS `resourcetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `resourcetype`
--

INSERT INTO `resourcetype` (`id`, `name`) VALUES
(1, 'text'),
(2, 'image'),
(3, 'video'),
(4, 'program');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `surName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` timestamp NULL DEFAULT NULL,
  `password` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `editDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createDateTime` timestamp NULL DEFAULT NULL,
  `userTypeId` int(11) NOT NULL,
  PRIMARY KEY (`id`,`userTypeId`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_user_userType_idx` (`userTypeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `surName`, `firstName`, `userName`, `dob`, `password`, `editDateTime`, `createDateTime`, `userTypeId`) VALUES
(1, 'Wang', 'Jake', 'jake', '2007-12-11 13:00:00', '123', '2013-09-29 04:43:43', '2013-09-29 04:43:43', 1),
(2, 'Luo', 'Stacey', 'stacey', '2005-01-22 13:00:00', '123', '2013-09-29 04:43:43', '2013-09-29 04:43:43', 1);

-- --------------------------------------------------------

--
-- Table structure for table `userresource`
--

CREATE TABLE IF NOT EXISTS `userresource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `resourceId` bigint(20) NOT NULL,
  `progress` float DEFAULT NULL,
  `editDateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_userResource_user1_idx` (`userId`),
  KEY `fk_userResource_resource1_idx` (`resourceId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `usertype`
--

CREATE TABLE IF NOT EXISTS `usertype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `usertype`
--

INSERT INTO `usertype` (`id`, `name`) VALUES
(1, 'student'),
(2, 'teacher'),
(3, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `version`
--

CREATE TABLE IF NOT EXISTS `version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  `editDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `version`
--

INSERT INTO `version` (`id`, `name`, `editDateTime`) VALUES
(3, '0.1.5', '2013-10-15 22:26:34');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `resource`
--
ALTER TABLE `resource`
  ADD CONSTRAINT `fk_resource_category1` FOREIGN KEY (`resourceCategoryId`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_resource_dynasty1` FOREIGN KEY (`dynastyId`) REFERENCES `dynasty` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_resource_resourcetype1` FOREIGN KEY (`resourceTypeId`) REFERENCES `resourcetype` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_userType` FOREIGN KEY (`userTypeId`) REFERENCES `usertype` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `userresource`
--
ALTER TABLE `userresource`
  ADD CONSTRAINT `fk_userResource_resource1` FOREIGN KEY (`resourceId`) REFERENCES `resource` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_userResource_user1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
